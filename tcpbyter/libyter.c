#include "libyter.h"


//Check if a string is in a valid ipv4 format. ddd.ddd.ddd.ddd
int is_ipv4_format (const char * ip)
{
	struct sockaddr_in sa;
	int result = inet_pton(AF_INET, ip, &(sa.sin_addr));
	return result;
}

//Check if a string is a valid number. 
//Could not find isnumber() in my machine,
int is_int_number(const char * str)
{
	int i, ret = 1;
	for (i=0; i<strlen(str); i++) { 
		if (isdigit(str[i]) == 0) {
			ret = 0;
			break;		
		}
	}
	return ret;
}


//Generates a random stream of bytes between 0 and 255.
unsigned char * rdm_bytestream (size_t num_bytes)
{
	unsigned char *stream = malloc (num_bytes);
	size_t i;

	for (i = 0; i < num_bytes; i++)
	{
		stream[i] = rand ();
	}
	return stream;
}


//Print all bytes of the bytestream in hex mode
void debug_bytestream (unsigned char * str, size_t num)
{
	int i;
	for (i=0; i<num; i++)
		printf("byte : %d - val : %x\n", i, str[i]);	
}


/* 
	Set a file descriptor (socket) either in blocking or non-blocking mode. 
	This means that now read/write's on the socket will do
	as much as they can and return immediately
	blocking : 0 -> nonblock
	blocking : 1 -> block
*/
int set_block_mode(int fd, int blocking) {
    /* Save the current flags */
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
        flags = 0;

    if (blocking == 1)
        flags &= ~O_NONBLOCK;
    else
        flags |= O_NONBLOCK;
    return fcntl(fd, F_SETFL, flags) != -1;
}


/*
	Receive data from a socket in several chunks of the specified size.
*/
ssize_t recv_from_socket(int sock, int chunk_size)
{
	int total_read = 0;
	char chunk[chunk_size];
	ssize_t bytes_read, res;
	fd_set readset;

	//Set non blocking socket.
	//set_block_mode(sock, 0);

	do {
		bzero(chunk, chunk_size);

		// Query the socket with select until it has available data to read */
		do {		
			FD_ZERO(&readset);
			FD_SET(sock, &readset);
			res = select(sock + 1, &readset, NULL, NULL, NULL);
		} while (res == -1 && errno == EINTR);

		// The socket has available data to be read */		
		if (res > 0) {
			if (FD_ISSET(sock, &readset)) {
		    bytes_read = recv(sock, chunk, sizeof(chunk), 0);
				total_read += bytes_read;

				if (bytes_read > 0) {
					#if DEBUG
						dprintf("[*] Server: received chunk of %zd bytes\n", bytes_read);	
					#endif
					continue;
				}

				if (bytes_read == 0){
					#if DEBUG
						dprintf("[*] Server: no more bytes to read\n");	
					#endif
					break;    	
				}
			}
		} 
 	} while (bytes_read > 0); 

	return total_read;
}


/*
	Reads all bytes from a socket in a single read into a big 
	buffer, hoping it will fit all in.
*/
ssize_t recv_from_socket_full(int sock, int bufsize)
{
	int total_read = 0;
	char buf[bufsize];
	ssize_t bytes_read;

	bzero(buf, bufsize);

	bytes_read = recv(sock, buf, sizeof(buf), 0);
	total_read = bytes_read;


	if (bytes_read < 0) {
		perror("ERROR reading from socket");
		exit(1);			
	}
	return total_read;
}


/*
	Send num amount of random bytes to a socket specified by sock.
*/
void send_rdm_bytes(int sock, int num)
{
	unsigned char * send_buf;
	int n;
	#if DEBUG
		dprintf("[*] Client: create random byte stream size: %d\n", num);	
	#endif
	send_buf = rdm_bytestream(num);

	//Set non blocking socket.
	//set_block_mode(sock, 0);

	/* Send message to the server */
  n = send(sock, send_buf, num, 0);
   
	#if DEBUG
		dprintf("[*] Client: %d bytes written to socket\n", n);	
	#endif   
	if (n < 0)
  {
  	perror("ERROR writing to socket");
    exit(1);
  }
}


void send_rdm_bytes_chunk(int sock, int num, int chunk_size) 
{
	unsigned char * send_buf;
	int n, chunks_written = 0, smaller, size_rem = num;
	#if DEBUG
		dprintf("[*] Client: create random byte stream size: %d\n", num);	
	#endif
	send_buf = rdm_bytestream(num);
	

	//Set non blocking socket.
	//set_block_mode(sock, 0);

	n = write(sock, send_buf, chunk_size);

	sleep(1);

	#if DEBUG
		dprintf("[*] Client: wrote chunk of %d bytes to socket\n", n);	
	#endif

	if (n == -1) {
		perror("ERROR writing to socket");
    exit(1);
	}

	chunks_written = chunks_written + n;
	size_rem = size_rem - chunk_size;

	while (size_rem > 0) {
		if (size_rem > chunk_size) {
			smaller = chunk_size;
		}
		else {
			smaller = size_rem;
		}

		n = write(sock, &send_buf[num-size_rem], smaller);

		sleep(1);

		#if DEBUG
			dprintf("[*] Client: wrote chunk of %d bytes to socket\n", n);	
		#endif	

		if( n == -1) {
		  perror("ERROR writing to socket");
		  exit(1);
		}
		size_rem = size_rem - chunk_size;
	}
}



//Create a client side socket and connect it to a host
int create_socket_client(char * host, int portno)
{
	 int sockfd;
   struct sockaddr_in serv_addr;
	 struct hostent *server;
   
	#if DEBUG
		dprintf("[*] Client: create socket()\n");	
	#endif

	 server = gethostbyname(host);

   if (server == NULL) {
   		fprintf(stderr,"ERROR, no such host\n");
      exit(1);
   }

   /* Create a socket point */
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0)
   {
      perror("ERROR opening socket");
      exit(1);
   }

	 //Initialize socket data.
	 bzero((char *) &serv_addr, sizeof(serv_addr));

   serv_addr.sin_family = AF_INET;
   bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
   serv_addr.sin_port = htons(portno);

	#if DEBUG
		dprintf("[*] Client: connect to server socket\n");	
	#endif
   /* Now connect to the server */
   if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
   {
      perror("ERROR connecting");
      exit(1);
   }
	return sockfd;
}



//Create a server side socket and bind it to a port
int create_socket_server(int serv_port)
{
	struct sockaddr_in serv_addr, cli_addr;
	int sockfd, newsockfd, portno, clilen;

	#if DEBUG
		dprintf("[*] Server: call to socket()\n");	
	#endif

	/* First call to socket() function */
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	
	if (sockfd < 0) {
		perror("ERROR opening socket");
		exit(1);
	}

	#if DEBUG
		dprintf("[*] Server: initialize socket structure\n");	
	#endif

	/* Initialize socket structure */
	bzero((char *) &serv_addr, sizeof(serv_addr));
  portno = serv_port;
  
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	#if DEBUG
		dprintf("[*] Server: bind the host address\n");	
	#endif
  
	/* Now bind the host address using bind() call.*/
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
  {
  	perror("ERROR on binding");
    exit(1);
  }
   
  /* Now start listening for the clients, here process will
  * go in sleep mode and will wait for the incoming connection
  */
  listen(sockfd,5); //maximum 5 clients in TIME_WAIT (backlog)
  clilen = sizeof(cli_addr);
	
  newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, (socklen_t *)&clilen);

	/* Accept actual connection from the client */
  if (newsockfd < 0) {
     perror("ERROR on accept");
     exit(1);
  }
  return newsockfd;
}

