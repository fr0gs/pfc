#include "libyter.h"

int port = 12345;
size_t chunk_size = 1024;
size_t whole_buffer = -1;


void print_help_sbyter()
{
	printf("Usage: sbyter [options]\n");
	printf("\t-c <chunk_size>: chunk size to split receiving data, default = 1024 bytes\n");
	printf("\t-s <whole_buffer>: specify the single read option and the buffer size\n");
	printf("\t-p <port> : listening port, default = 12345\n");
}


void parse_arguments(int argc, char **argv) {

	if (argv[1] != NULL && !strncmp(argv[1], "-h", 2)) {
		print_help_sbyter();
		exit(0);
	}

	int i;
	for (i=0; i<argc; i++) {
		if (!strncmp(argv[i], "-p", 2)) { 
			if (argv[i+1] != NULL) {
				if (is_int_number(argv[i+1])) {
					port = atoi(argv[i+1]);
					#if DEBUG
						dprintf("[*] Server: port specified: %d\n", port);	
					#endif	
				}
				else {
					printf("++ Port is not a number\n\n");
					print_help_sbyter();
				}
			}
		}
		else if (!strncmp(argv[i], "-c", 2)) { 
			if (argv[i+1] != NULL) {
				int csize = 0, pos = strlen(argv[i+1]);
				char * aux = strndup(argv[i+1], pos); // 123M -> 123
				if (argv[i+1][pos-1] == 'K') {
					csize = atoi(aux)*BSIZE;
				}
				else if (argv[i+1][pos-1] == 'M') {
					csize = atoi(aux)*BSIZE*BSIZE;
				}
				else {
					csize = atoi(aux);
				}

				chunk_size = csize;
				#if DEBUG
						dprintf("[*] Server: chunk size: %zd\n", chunk_size);	
				#endif	
			}
		}
		else if (!strncmp(argv[i], "-s", 2)) { 
			if (argv[i+1] != NULL) {
				int wbuff = 0, pos = strlen(argv[i+1]);
				char * aux = strndup(argv[i+1], pos); // 123M -> 123
				if (argv[i+1][pos-1] == 'K') {
					wbuff = atoi(aux)*BSIZE;
				}
				else if (argv[i+1][pos-1] == 'M') {
					wbuff = atoi(aux)*BSIZE*BSIZE;
				}
				else {
					wbuff = atoi(aux);
				}

				whole_buffer = wbuff;
				#if DEBUG
						dprintf("[*] Server: whole buffer: %zd\n", whole_buffer);	
				#endif	
			}
		}		
	}
}


int main(int argc, char ** argv)
{
	int serv_sock, total_read = 0;

	parse_arguments(argc, argv);	

	printf("++ Server listening in port: %d\n", port);
	if (chunk_size != -1 )
		printf("++ Server receive chunk size: %zd\n", chunk_size);
	else
		printf("++ Server receive full buffer, test case: %zd\n bytes", whole_buffer);

	//Create the server socket in the specified port
	serv_sock = create_socket_server(port);
	
	//Receive data from socket.
	if (whole_buffer == -1)
		//Reads from the socket specifying a chunk size
		total_read = recv_from_socket(serv_sock, chunk_size);
	else 
		//Tries to read all data from socket in a single one-buffer read.
		//Test way, you need to know how many data will the sender send (1Mb default).
		total_read = recv_from_socket_full(serv_sock, whole_buffer);

	

	#if DEBUG
		dprintf("[*] Server: total bytes read: %d\n", total_read);	
	#endif


	/* Close socket */
	if ((close(serv_sock)) < 0)
	{
		perror("ERROR closing socket");
    exit(1);
	}	

	#if DEBUG
		dprintf("[*] Server: exit program\n");	
	#endif

  return 0;
}
