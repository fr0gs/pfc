#include <time.h>
#include "libyter.h"


//Global variables
int nbytes = (BSIZE*BSIZE); //1M
int port = 12345;
int chunk_size = -1;
char * host = NULL;



void print_help_cbyter()
{
	printf("Usage: cbyter [options] host\n");
	printf("\t-b <bytes>: amount of bytes to send. Format [num][K|M], default = 1Mbyte\n");
	printf("\t-c <chunk_size>: chunk size to split sending data, default = no split\n");
	printf("\t-p <port> : port to send traffic, default = 12345\n");
}


void parse_arguments(int argc, char **argv)
{
	int i;

	if (argv[1] != NULL && !strncmp(argv[1], "-h", 2)) {
		print_help_cbyter();
		exit(0);
	}

	for (i=0; i<argc; i++) {
		if (is_ipv4_format(argv[i])) {
			host = strdup(argv[i]);
			#if DEBUG
				dprintf("[*] Client: host specified: %s\n", host);	
			#endif	
		}
		else if (!strncmp(argv[i], "-p", 2)) { 
			if (argv[i+1] != NULL) {
				if (is_int_number(argv[i+1])) {
					port = atoi(argv[i+1]);
					#if DEBUG
						dprintf("[*] Client: port specified: %d\n", port);	
					#endif	
				}
				else {
					printf("++ Port is not a number\n\n");
					print_help_cbyter();
				}
			}
		}
		else if (!strncmp(argv[i], "-b", 2)) { 
			if (argv[i+1] != NULL) {
				int numbytes = 0, pos = strlen(argv[i+1]);
				char * aux = strndup(argv[i+1], pos); // 123M -> 123
				if (argv[i+1][pos-1] == 'K') {
					numbytes = atoi(aux)*BSIZE;
				}
				else if (argv[i+1][pos-1] == 'M') {
					numbytes = atoi(aux)*BSIZE*BSIZE;
				}
				else {
					numbytes = atoi(aux);
				}
				nbytes = numbytes;
			}
		}
		else if (!strncmp(argv[i], "-c", 2)) { 
			if (argv[i+1] != NULL) {
				int csize = 0, pos = strlen(argv[i+1]);
				char * aux = strndup(argv[i+1], pos); // 123M -> 123
				if (argv[i+1][pos-1] == 'K') {
					csize = atoi(aux)*BSIZE;
				}
				else if (argv[i+1][pos-1] == 'M') {
					csize = atoi(aux)*BSIZE*BSIZE;
				}
				else {
					csize = atoi(aux);
				}
				chunk_size = csize;
			}
		}
	}
}


int main(int argc, char **argv)
{
	 int client_sock;

	//Initialize the seed to generate random bytes.
	srand ((unsigned int) time (NULL));
   
	if (argc < 2) {
		print_help_cbyter();
		exit(0);
	}

	parse_arguments(argc, argv);
    
	//Create client socket 
	client_sock = create_socket_client(host, port);  
  
	if (chunk_size == -1) {
		#if DEBUG
			dprintf("[*] Client: single write in socket\n");	
		#endif	
		//Send specific amount of random bytes.
		send_rdm_bytes(client_sock, nbytes);
	}
	else {
	  #if DEBUG
			dprintf("[*] Client: split write in socket\n");	
		#endif
		send_rdm_bytes_chunk(client_sock, nbytes, chunk_size);
	}
	
	/* Close socket */
	if ((close(client_sock)) < 0)
	{
		perror("ERROR closing socket");
    exit(1);
	}		

	#if DEBUG
		dprintf("[*] Client: exit program\n");	
	#endif

	return 0;
}
