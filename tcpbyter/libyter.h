#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include "config.h"


//defines
#define DEBUG 1
#define BSIZE 1024
#define dprintf(format, ...) \
	 do { fprintf (stderr, format, ## __VA_ARGS__); } while (0)


//general functions
int is_ipv4_format (const char * ip);
int is_int_number(const char * str);
unsigned char * rdm_bytestream (size_t num_bytes);
void debug_bytestream (unsigned char * str, size_t num);
int set_block_mode(int fd, int blocking);


//socket functions
ssize_t recv_from_socket(int sock, int chunk_size);
ssize_t recv_from_socket_full(int sock, int bufsize);
void send_rdm_bytes(int sock, int num);
void send_rdm_bytes_chunk(int sock, int num, int chunk_size);
int create_socket_client(char * host, int portno);
int create_socket_server(int serv_port);
