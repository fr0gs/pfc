#ifndef UTIL_H 
#define UTIL_H
#include "ns3/network-module.h"
#include "ns3/core-module.h"


namespace ns3 {
void RunIp (Ptr<Node> node, Time at, std::string str);

void RunIperf(Ptr<Node> node, Time start, Time stop, std::string args);

void RunBinary(Ptr<Node> node, Time start, Time stop, std::string args, std::string which);

void AddAddress (Ptr<Node> node, Time at, const char *name, const char *address);

std::string Ipv4AddressToString (Ipv4Address ad);

void PrintNodeIpv4Addresses(Ptr<Node> node);

std::string GetNodeIpv4Address (Ptr<Node> node);

inline bool file_exists (const std::string& name);

int getRandInteger (int min, int max, int seed);

float getRandFloat (float min, float max, int seed);

void PrintTcpFlags (std::string key, std::string value);

}

#endif // UTIL_H
