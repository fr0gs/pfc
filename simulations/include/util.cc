#include "util.h"
#include "ns3/dce-module.h"

using namespace std;

namespace ns3 {

void RunIp (Ptr<Node> node, Time at, std::string str)
{
  DceApplicationHelper process;
  ApplicationContainer apps;
  process.SetBinary ("ip");
  process.SetStackSize (1 << 16);
  process.ResetArguments ();
  process.ParseArguments (str.c_str ());
  apps = process.Install (node);
  apps.Start (at);
}

/********************************************************
	Runs iperf accordingly with the command line arguments
	provided.
********************************************************/
void RunIperf(Ptr<Node> node, Time start, Time stop, std::string args)
{
	DceApplicationHelper dce;
	ApplicationContainer apps;
	
	dce.SetBinary("iperf");
  dce.SetStackSize (1 << 20);
  dce.ResetArguments ();
  dce.ParseArguments (args.c_str ());
  apps = dce.Install(node);
  apps.Start (start);

	if (stop.IsPositive()){
  	apps.Stop (stop);
	}
}


/********************************************************
	Runs a native binary with DCE with the arguments provided.
	I do not check if the binary exists in the DCE_PATH, so you
	better check that you wrote the name properly.
********************************************************/
void RunBinary(Ptr<Node> node, Time start, Time stop, std::string args, std::string which)
{
	DceApplicationHelper dce;
	ApplicationContainer apps;
	
	dce.SetBinary(which);
  dce.SetStackSize (1 << 20);
  dce.ResetArguments ();

	if (args != "")
		dce.ParseArguments (args.c_str ());

  apps = dce.Install(node);
  apps.Start (start);

	if (stop.IsPositive()){
  	apps.Stop (stop);
	}
}



/*
	Check if file exists.
	"name" is an absolute path, so well.
*/
inline bool file_exists (const std::string& name) 
{
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}


void AddAddress (Ptr<Node> node, Time at, const char *name, const char *address)
{
  std::ostringstream oss;
  oss << "-f inet addr add " << address << " dev " << name;
  RunIp (node, at, oss.str ());
}


std::string Ipv4AddressToString (Ipv4Address ad)
{
  std::ostringstream oss;
  ad.Print (oss);
  return oss.str ();
}

/********************************************************
	Prints all Ipv4Addresses from a node interfaces in a 
	single loop to standard output
********************************************************/
void PrintNodeIpv4Addresses(Ptr<Node> node)
{
	std::cout << "node ID: " << node->GetId() << "\n";
	int nDevices = node->GetNDevices();
  Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> (); // Get Ipv4 instance of the node
	for (int i=0; i<nDevices; i++){
  	Ipv4Address addr = ipv4->GetAddress (i, 0).GetLocal (); // Get Ipv4InterfaceAddress of xth interface.
		std::cout << "\t-> " << "interface: " << i << " -- " << addr << "\n";
	}
}

/********************************************************
	I assume the node will always be an end node, otherwise 
	I would have to choose wich node interface I would want 
	to extract its address.
********************************************************/
std::string GetNodeIpv4Address (Ptr<Node> node)
{
	Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> (); // Get Ipv4 instance of the node
  Ipv4Address addr = ipv4->GetAddress (0, 0).GetLocal (); // Get Ipv4InterfaceAddress of xth interface.
	std::string str = Ipv4AddressToString (addr);
	return str;
}

/*
	Get random integer between a [min, max] interval,
	initializing the seed with current time.	
*/
int getRandInteger (int min, int max, int seed) 
{
	std::srand(seed);
	int output = min + (std::rand() % (int)(max - min + 1));
	return output;
}


float getRandFloat (float min, float max, int seed)
{
	std::srand(static_cast <unsigned> (seed));
	float ret = min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max-min)));
	return ret;
}



void PrintTcpFlags (std::string key, std::string value)
{
	printf("%s=%s\n", key.c_str(), value.c_str());
	//NS_LOG_INFO (key << "=" << value);
}

}
