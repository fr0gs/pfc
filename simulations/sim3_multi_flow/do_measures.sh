#!/bin/bash

bc="1024K"
bbg="4096K"
alg=$1
error=$2

ker=$(pwd | awk -F'/' '{ print $5 }' | awk -F'_' '{ print $NF }')
rm -rf $ker
mkdir -p $ker


if [ "$#" -ne 2 ]; then
	echo "$ ./do_measures.sh <tcp_congestion_control> <error(%)>"
	echo "Taking default values. reno/0%"
	alg="reno"
	error="0"
fi

for i in `seq 2 2 4`
do
  echo "Main flow: $bc bytes"	
  echo "Background flow: $bbg bytes"	
	make clean &>/dev/null
	wafdce --run "sim3_multi_flow --nodes=$i --bc=$bc --bbg=$bbg --algorithm=$alg --error=$error --run=$i"
	echo "+++---------------------------------+++"
	echo "Generate graphs with \"grap_generator.sh\""
	graph_generator.sh csma-0-0.pcap &>/dev/null # Makes dir graphs/ 
	declare namedir="graphs_"$i                    # Graph name for this sim. (i.e: graphs_2)
	echo "Move graphs/ to $namedir"
	mv graphs/ $namedir
	mv $namedir $ker
	unset namedir	
done
