#!/bin/bash

# reno bic cubic westwood highspeed hybla htcp vegas veno scalable lp yeah illinois


alg=$1
dest=$2
error=$3

if [ "$#" -ne 3 ]; then
	echo "Usage: $ ./sim3.sh <tcp_congestion_control> <dest_dir> <error(%)>"
	exit -1
fi

direc=$dest"/"
if [ ! -d "$direc" ]; then
  mkdir $direc
fi

direc=$direc"sim3_multi_flow/"
if [ ! -d "$direc" ]; then
  mkdir $direc
fi


# Now we have a directory with the name of the algorithm. (i.e "reno/")
direc=$direc$alg
if [ ! -d "$direc" ]; then
  mkdir $direc
fi


kernels=( "2.6" "3.7" "3.10" "3.12" ) #"3.14" )
#algoritmos=( "reno" "bic" "cubic" "westwood" "highspeed" "hybla" "htcp" "vegas" "veno" "scalable" "lp" "yeah" "illinois" )


for kernelver in "${kernels[@]}"
do
	echo "[+] Change to directory: "$kernelver
	cdmysc $kernelver
	cd sim3_multi_flow/
	kernv=$(pwd | awk -F'/' '{ print $5 }' | awk -F'_' '{ print $2 }') # Name of dir (i.e. "2.3.6")
	./do_measures.sh $alg $error
	cp -Rf $kernv $direc
	cd -
done

unset fin
unset alg
unset direc
unset dest
unset error
unset kernv
