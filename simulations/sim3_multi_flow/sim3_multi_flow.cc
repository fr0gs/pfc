#include "ns3/network-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/dce-module.h"
#include "ns3/applications-module.h"
#include "ns3/drop-tail-queue.h"
#include "ns3/codel-queue.h"
#include "ns3/csma-module.h"
#include "../include/util.h"

#include <fstream>

#define DEBUG 1

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("sim_testshit");

int main (int argc, char *argv[])
{
	string bc = "64K";
	string bbg = "128K";
	string qtype = "droptail";
	string algorithm = "reno";
	int nodeNumber = 2; //Default two nodes in the client side.
	int runNumber = 1;
	double error = 0;

  CommandLine cmd;
	cmd.AddValue ("nodes", "Number of nodes", nodeNumber);
	cmd.AddValue ("bc", "Bytes Client", bc);
	cmd.AddValue ("bbg", "Bytes background", bbg);
	cmd.AddValue ("algorithm", "Congestion control algorithm", algorithm);
	cmd.AddValue ("error", "Error Rate to simulate", error);
	cmd.AddValue ("run", "Run Number", runNumber);

  cmd.Parse (argc, argv);

	//Set run number (independent executions)
	RngSeedManager::SetRun (runNumber);

	int totalNodes = nodeNumber+3; //Client side + 2 routers + server

  //Creates all the nodes together.
  NodeContainer nodes;
  nodes.Create (totalNodes);
	

	NodeContainer leftNodes;
  for (int i=0; i<=nodeNumber; i++) {
  	 leftNodes.Add (nodes.Get (i));
	}

	NodeContainer centralNodes = NodeContainer (nodes.Get(nodeNumber), nodes.Get(nodeNumber+1));
	NodeContainer rightNodes = NodeContainer (nodes.Get(nodeNumber+1), nodes.Get(nodeNumber+2));


#ifdef KERNEL_STACK
  //Installs the linux kernel in all nodes.
  DceManagerHelper dceManager;
  dceManager.SetNetworkStack ("ns3::LinuxSocketFdFactory", "Library", StringValue ("liblinux.so"));
  dceManager.Install (nodes);

  LinuxStackHelper stack;
  stack.Install (nodes);
#else
  NS_LOG_ERROR ("Linux kernel stack for DCE is not available. build with dce-linux module.");
  return 0;
#endif
	
	CsmaHelper csma;
	NetDeviceContainer leftDevices, centralDevices, rightDevices;

	//Set the left channel attributes
	csma.SetChannelAttribute ("DataRate", StringValue ("1Gbps"));
  csma.SetChannelAttribute ("Delay", StringValue ("1ms"));
	leftDevices = csma.Install (leftNodes);
	
	//Set the central channel attributes
	csma.SetChannelAttribute ("DataRate", StringValue("200Mbps"));
	csma.SetChannelAttribute ("Delay", StringValue("98ms"));	
	centralDevices = csma.Install(centralNodes);

	//Set the right channel attributes
	csma.SetChannelAttribute ("DataRate", StringValue ("1Gbps"));
	csma.SetChannelAttribute ("Delay", StringValue("1ms"));
	rightDevices = csma.Install(rightNodes);


	//Get the CsmaNetDevice associated to the first router.
	Ptr<NetDevice> dev = centralDevices.Get(0);
	Ptr<CsmaNetDevice> router = dev->GetObject<CsmaNetDevice> ();

	Ptr<NetDevice> dev1 = centralDevices.Get(1);
	Ptr<CsmaNetDevice> router1 = dev1->GetObject<CsmaNetDevice> ();

	//Create the queue.
	Ptr<Queue> qt;
	Ptr<Queue> qt1;
	if ( qtype == "droptail" ) {
		//Create a DropTailQueue
		qt = CreateObject<DropTailQueue> (); 
		qt1 = CreateObject<DropTailQueue> (); 
		//DropTailQueue uses packets by default.
	}
	else {
		NS_LOG_ERROR ("ERROR: queue type not supported");
    exit(-1);
	}

	qt->SetAttribute ("MaxPackets", UintegerValue(100));
	qt1->SetAttribute ("MaxPackets", UintegerValue(100));

	//Install the queue both routers.
	router->SetQueue(qt);
	router1->SetQueue(qt1);

	//Create a RateErrorModel for the receive device in Client and vary its param
	//Ptr<RateErrorModel> em = CreateObject<RateErrorModel> ();
	
	//em->SetAttribute ("ErrorRate", DoubleValue (error));
	//em->SetAttribute ("ErrorUnit", EnumValue(RateErrorModel::ERROR_UNIT_PACKET)); //byte by default

	//Install the RateErrorModel both central routers.
	//centralDevices.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em));

  //Assigns IP addresses to the three different networks.
  Ipv4AddressHelper address;
  
  address.SetBase("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer leftInterfaces =  address.Assign (leftDevices);
  
  address.SetBase("192.168.2.0", "255.255.255.0");
  Ipv4InterfaceContainer centralInterfaces = address.Assign (centralDevices);

	address.SetBase("192.168.3.0", "255.255.255.0");
	Ipv4InterfaceContainer rightInterfaces = address.Assign (rightDevices);


	//Populate routing tables.
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  LinuxStackHelper::PopulateRoutingTables ();


	//First of all launch the iperf server before all clients.
	float timeServer = 0.5;
	RunIperf(nodes.Get (totalNodes-1), Seconds (timeServer), Seconds(-1), "-s");


	//Establish iperf parameters for all the nodes 1 .. nodeNumer-1
	//All those nodes will start sending before our client.
	string str;
	str = GetNodeIpv4Address (nodes.Get (totalNodes-1)); // Get the Ipv4Address of the last node.

	ostringstream args;
	args.str("");
	args.clear();
  args << "-c " << str << " -n " << bbg << " -F " << "/dev/urandom";

	cout << "Algorithm: " << algorithm << "\n";
	cout << "Nodes in client side: " << nodeNumber << "\n";
	cout << "Error Rate: " << error << "%" << "\n";
	cout << "Run Number: " << runNumber << "\n";

	//Launch all the background traffic after
	float startBg; 
  for (int i=1; i<nodeNumber; i++)
	{
		startBg = timeServer+i-0.5;
		cout << "node number: " << i << "|| starts at seconds: " << startBg << "|| sends " << bbg << " bytes\n";
		RunIperf (nodes.Get (i), Seconds (startBg), Seconds (-1), args.str ());
	}
	cout << "\n";

	//Establish iperf parameters for the main flow node.
	args.str("");
	args.clear();
  args << "-c " << str << " -n " << bc << " -F " << "/dev/urandom";

	startBg += 0.5;

	
	//Now the client MUST start 0.5 seconds after everyone.
	cout << "node number: 0" << "|| starts at seconds: " << startBg << "|| sends " << bc << " bytes\n";
	

	//Launch first main node
	RunIperf (nodes.Get (0), Seconds (startBg), Seconds (-1), args.str ());

	//Set sysctl parameters
	stack.SysctlSet (nodes.Get (0), ".net.ipv4.tcp_congestion_control", algorithm);


	csma.EnablePcapAll ("csma");
  Simulator::Stop (Hours (10));
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}
