# README #
## Short Transfer Simulation ##

### Description ###
* **Iperf** to generate traffic.
* This is a simulation of variable number nodes trying to emulate multiple flows sharing a connection.

        	C1 ---|   
        	C2 ---|   
            	  |- R1 --- R2 - Server
        	..    |
        	Cn ---|

* There is no packet loss in thisscenario, congestion is reached with sharing flows.
* Two different sets of nodes sending traffic from the client side: 2, and 4 nodes.
* C1 main flow, 1024 bytes.
* C2 .. Cn background flows, to create congestion. 4096 bytes/each.
* Queue Size = 100, both router.
* Queue disciplines: Droptail.
* C1..Cn <-> R1 GigabitEthernet-like network.

        DataRate: 1Gbps
        Delay: 1ms
		
* R1 <-> R2 slow link.

        Datarate: 200Mbps
        Delay: 98ms
		
* RNG seed changed  + run number in each execution. Independent runs.
* MSS = 1460 bytes

Measure:
For each kernel version (graphs): 2.6 / 3.7 / 3.10 / 3.12
	1. Delay
	2. Throughput
	3. Time sequence


### Install ###
* Copy the **include/** folder in the **myscripts/** directory in your ns-3-dce project.
* Copy the **sim3_multi_flow** folder to the **myscripts/** folder in your ns-3-dce project.
* Copy **graph_generator.sh** into **/usr/bin/** or create a function that calls it wherever it is.
	This script uses **captcp** and **tcptrace** tools to generate the graphs.
* Copy **tpinf-stat** jar file into **/usr/bin** , or create a function that calls it wherever it is, mine is:

        function tpinf-stat {
		  local CWD="$PWD"
		  cd $PFCDIR"/scripts/"
		  java -jar tpinf-stat $CWD"/""$@"
		  cd - &>/dev/null
	    }
	    export -f tpinf-stat

* Have **wafdce** function defined in .bashrc, this function makes you able to execute a wafdce script from
  the same directory where you are.

        function wafdce {
    		local CWD="$PWD"
    		local DCENS=$(pwd | awk -F'/'  '{ for(i = 1; i <= NF-2; i++) printf("%s%s", $i, i == NF - 2 ? "\n" : OFS)}' OFS="/")"/"
    		cd $DCENS
    		./waf --cwd="$CWD" "$@" 
    		cd - &>/dev/null 
	    }
	    export -f wafdce

### Execute ###
* **Sim3.sh** script comprehend a different behaviour than the **sim2.sh** simulation scripts. In this case instead of having
  a single script that performs the task of execution and measure taking, another script called **do_measures.sh** is the one
  in charge of executing the simulation and call the graph generation script. **Sim3.sh** will only change within projects with
  different kernel versions and call **do_measures.sh** to do the job.
	Each kernel version is in a different directory with it's own **myscripts/** directory. It is structured
	in a way where there is four copies of the ns-3-dce project, each one using a different kernel version.
	This is done this way in order to speed up the execution of tests when changing from one version to
	the other, having to deal with the nuisance of four copies of the simulation distributed among projects is 
	always better than rebuilding project each time with a different kernel, which in turn I am not sure
	if that can occur without arising several errors when re-executing simulations. So, approach structure is this way:

			2.6/myscripts/simulation
			3.7/myscripts/simulation
			3.10/myscripts/simulation
			3.12/myscripts/simulation
	
	The **cdmysc** function called in the script just changes from one **myscripts/** directory to the other. Routes will change
  between computers, but this is mine as an orientation:

	    function cdmysc {
		    local cdir=$HOME"/gits/proyecto_"
		    if [[ $1 == "2.6" || $1 == "3.7" || $1 == "3.10" || $1 == "3.12" || $1 == "3.14" ]]; then
			    if [[ $1 == "2.6"  ]]; then
				    cdir=$cdir$1".36/source/ns-3-dce/myscripts/"
			    else
				    cdir=$cdir$1".0/source/ns-3-dce/myscripts/"
			    fi
		    cd $cdir
		    else
			    echo "Error cdmysc: unknown directory value"
		    fi
	    }
	    export -f cdmysc

	    $ ./sim2.sh <tcp_congestion_control> <dest_dir> <error(%)>

* **Sim3_averages.sh** executes the sim3_multi_flow simulation 6 times, gets the average
  throughput/delay data, and creates a data file for each one.
  Output is then copied to the specified directory. 

* For a single execution of the simulation:

	    $ wafdce --run "sim3_multi_flow"
			
			sim3_multi_flow [Program Arguments] [General Arguments]

			Program Arguments:
    			--nodes:      Number of nodes [2]
	            --bc:         Bytes Client [64K]
                --bbg:        Bytes background [128K]
                --algorithm:  Congestion control algorithm [reno]
                --error:      Error Rate to simulate [0]
                --run:        Run Number [1]
