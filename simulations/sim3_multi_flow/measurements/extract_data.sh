#!/bin/bash

# This script will extract all throughput and delay data from the sim3_multi_flow_data/ directory.
# File paths are that long because I'd rather copy and structure all data from the root dir
# instead of "cd <dir>" and "cd -" in each phase of the loops.

#echo "@@ Generate all the png graphs.."
#find . -type f -name 'Makefile' -printf '%h\n' | xargs -i bash -c 'cd "$0"; make png; cd -' {}

red='\033[1;31m'
pink='\033[1;35m'
green='\033[1;32m'
yellow='\033[1;33m'
blue='\033[1;34m'
orange='\033[0;33m'
nc='\033[0m' # No Color

kernels=( "2.6.36" "3.7.0" "3.10.0" "3.12.0" )


# Make the sim3_data_extracted/ folder
# Make the delays/ folder.
destdir="sim3_data_extracted"
rm -rf $destdir
mkdir -p $destdir
rm -rf delays
mkdir "delays" # Folder for delays (common to all algorithms)


# Create plot file for delays, both for 2, 4, 6 and 8 nodes.
plotfile_delays="delays/plot_delays.plt"
touch $plotfile_delays

echo -e "${blue}[*] ${orange}Starting analysis${nc}"

for alg in `ls sim3_multi_flow_data` 
do
		# Create the tput_<alg>_comparison.txt file, to have all data with kernel / avg. throughput
		# well structured.
		echo -e "${blue}[*] ${red}Change to algorithm directory $alg ${nc}"
		mkdir $destdir"/"$alg
		mkdir $destdir"/"$alg"/tput"
		tempres=$destdir"/"$alg"/tput/tput_"$alg"_comparison.txt"
		touch $tempres
		echo "THROUGHPUT COMPARISON : $alg" >> $tempres
		echo "Results are measured in Kb/s , numbers are the averages" >> $tempres
		echo "Structure: kernel version / average throughput" >> $tempres
		echo "++++++++++++++++++++++++++++++++++++" >> $tempres

		# Gather throughput files for 2 nodes
		echo -e "  ${blue}[*] ${yellow} Doing the 2 node things ${nc}"
		echo -e "(**) -- 2 nodes" >> $tempres
		for kernv in ${kernels[@]}
		do
			echo -e "   ${blue}[*] ${green}Change to kernel directory $kernv ${nc}"
			tputfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_2/captcp/tput/throughput.data"
			tputpngfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_2/captcp/tput/throughput.png"
			destputfile=$destdir"/"$alg"/tput/tput_"$alg"_"$kernv"_2.data"
			destputpngfile=$destdir"/"$alg"/tput/tput_"$alg"_"$kernv"_2.png"
			cp -Rf $tputfile $destputfile
			cp -Rf $tputpngfile $destputpngfile
			avg_tput=$(tpinf-stat $tputfile | awk 'END{ print $3 }')
			echo -e "     $kernv  / $avg_tput" >> $tempres
		done
		
		echo "" >> $tempres

		# Gather throughput files for 4 nodes
		echo -e "(**) -- 4 nodes" >> $tempres
		echo -e "  ${blue}[*] ${yellow} Doing the 4 node things ${nc}"
		for kernv in ${kernels[@]}
		do
			echo -e "   ${blue}[*] ${green}Change to kernel directory $kernv ${nc}"
			tputfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_4/captcp/tput/throughput.data"
			tputpngfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_4/captcp/tput/throughput.png"
			destputfile=$destdir"/"$alg"/tput/tput_"$alg"_"$kernv"_4.data"
			destputpngfile=$destdir"/"$alg"/tput/tput_"$alg"_"$kernv"_4.png"
			cp -Rf $tputfile $destputfile
			cp -Rf $tputpngfile $destputpngfile
			avg_tput=$(tpinf-stat $tputfile | awk 'END{ print $3 }')
			echo -e "     $kernv  / $avg_tput" >> $tempres
		done

		echo "" >> $tempres

		# Gather throughput files for 6 nodes
		echo -e "(**) -- 6 nodes" >> $tempres
		echo -e "  ${blue}[*] ${yellow} Doing the 6 node things ${nc}"
		for kernv in ${kernels[@]}
		do
			echo -e "   ${blue}[*] ${green}Change to kernel directory $kernv ${nc}"
			tputfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_6/captcp/tput/throughput.data"
			tputpngfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_6/captcp/tput/throughput.png"
			destputfile=$destdir"/"$alg"/tput/tput_"$alg"_"$kernv"_6.data"
			destputpngfile=$destdir"/"$alg"/tput/tput_"$alg"_"$kernv"_6.png"
			cp -Rf $tputfile $destputfile
			cp -Rf $tputpngfile $destputpngfile
			avg_tput=$(tpinf-stat $tputfile | awk 'END{ print $3 }')
			echo -e "     $kernv  / $avg_tput" >> $tempres
		done

		echo "" >> $tempres

		# Gather throughput files for 8 nodes
		echo -e "(**) -- 8 nodes" >> $tempres
		echo -e "  ${blue}[*] ${yellow} Doing the 8 node things ${nc}"
		for kernv in ${kernels[@]}
		do
			echo -e "   ${blue}[*] ${green}Change to kernel directory $kernv ${nc}"
			tputfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_8/captcp/tput/throughput.data"
			tputpngfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_8/captcp/tput/throughput.png"
			destputfile=$destdir"/"$alg"/tput/tput_"$alg"_"$kernv"_8.data"
			destputpngfile=$destdir"/"$alg"/tput/tput_"$alg"_"$kernv"_8.png"
			cp -Rf $tputfile $destputfile
			cp -Rf $tputpngfile $destputpngfile
			avg_tput=$(tpinf-stat $tputfile | awk 'END{ print $3 }')
			echo -e "     $kernv  / $avg_tput" >> $tempres
		done


		# Create plot script file for throughputs of one algorithm accross kernel versions
		plotfile_alg=$destdir"/"$alg"/tput/plot_tput_"$alg".plt"
			
		pngoutput="tput_"$alg"_bykernel_2.png"

		echo -e "reset" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "set term png" >> $plotfile_alg
		echo -e "set logscale xy" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "set output \"$pngoutput\"" >> $plotfile_alg
		echo -e "set ylabel \"Throughput (Kb/s)\"" >> $plotfile_alg
		echo -e "set xlabel \"Time (seconds) \"" >> $plotfile_alg
		echo -e "set title \"Throughput : $alg -- 2 nodes -- logscale\"" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "set grid" >> $plotfile_alg
		echo -e "set autoscale xfix" >> $plotfile_alg
		echo -e "set offsets 1,1,0,0" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "plot 'tput_"$alg"_2.6.36_2.data' using 1:2 with lines title \"2.6.36\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.7.0_2.data' using 1:2 with lines title \"3.7.0\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.10.0_2.data' using 1:2 with lines title \"3.10.0\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.12.0_2.data' using 1:2 with lines title \"3.12.0\" \\" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		
		pngoutput="tput_"$alg"_bykernel_4.png"

		echo -e "set output \"$pngoutput\"" >> $plotfile_alg
		echo -e "set title \"Throughput : $alg -- 4 nodes -- logscale\"" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "plot 'tput_"$alg"_2.6.36_4.data' using 1:2 with lines title \"2.6.36\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.7.0_4.data' using 1:2 with lines title \"3.7.0\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.10.0_4.data' using 1:2 with lines title \"3.10.0\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.12.0_4.data' using 1:2 with lines title \"3.12.0\" \\" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "" >> $plotfile_alg

		pngoutput="tput_"$alg"_bykernel_6.png"

		echo -e "set output \"$pngoutput\"" >> $plotfile_alg
		echo -e "set title \"Throughput : $alg -- 6 nodes -- logscale\"" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "plot 'tput_"$alg"_2.6.36_6.data' using 1:2 with lines title \"2.6.36\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.7.0_6.data' using 1:2 with lines title \"3.7.0\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.10.0_6.data' using 1:2 with lines title \"3.10.0\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.12.0_6.data' using 1:2 with lines title \"3.12.0\" \\" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "" >> $plotfile_alg

		pngoutput="tput_"$alg"_bykernel_8.png"

		echo -e "set output \"$pngoutput\"" >> $plotfile_alg
		echo -e "set title \"Throughput : $alg -- 8 nodes -- logscale\"" >> $plotfile_alg
		echo -e "" >> $plotfile_alg
		echo -e "plot 'tput_"$alg"_2.6.36_8.data' using 1:2 with lines title \"2.6.36\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.7.0_8.data' using 1:2 with lines title \"3.7.0\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.10.0_8.data' using 1:2 with lines title \"3.10.0\", \\" >> $plotfile_alg
		echo -e "     'tput_"$alg"_3.12.0_8.data' using 1:2 with lines title \"3.12.0\" \\" >> $plotfile_alg
		

		# Retrieve delays for 2 nodes
		echo -e "    ${pink}[*] Delays for $alg with 2 nodes ${nc}"
		delaydata="delays/delay_"$alg"_2.data"
		rm -f $delaydata
		touch $delaydata 
		count=1
		for kernv in ${kernels[@]}
		do
			tsgfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_2/captcp/tsg/seq.data"
			delaytime=$(cat $tsgfile | awk 'END { print $1 }')
			echo -e "$count\t$delaytime" >> $delaydata
			count=$((count+1))
		done

		# Retrieve delays for 4 nodes
		echo -e "    ${pink}[*] Delays for $alg with 4 nodes ${nc}"
		delaydata="delays/delay_"$alg"_4.data"
		rm -f $delaydata
		touch $delaydata 
		count=1
		for kernv in ${kernels[@]}
		do
			tsgfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_4/captcp/tsg/seq.data"
			delaytime=$(cat $tsgfile | awk 'END { print $1 }')
			echo -e "$count\t$delaytime" >> $delaydata
			count=$((count+1))
		done

		# Retrieve delays for 6 nodes
		echo -e "    ${pink}[*] Delays for $alg with 6 nodes ${nc}"
		delaydata="delays/delay_"$alg"_6.data"
		rm -f $delaydata
		touch $delaydata 
		count=1
		for kernv in ${kernels[@]}
		do
			tsgfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_6/captcp/tsg/seq.data"
			delaytime=$(cat $tsgfile | awk 'END { print $1 }')
			echo -e "$count\t$delaytime" >> $delaydata
			count=$((count+1))
		done

		# Retrieve delays for 8 nodes
		echo -e "    ${pink}[*] Delays for $alg with 8 nodes ${nc}"
		delaydata="delays/delay_"$alg"_8.data"
		rm -f $delaydata
		touch $delaydata 
		count=1
		for kernv in ${kernels[@]}
		do
			tsgfile="sim3_multi_flow_data/"$alg"/"$kernv"/graphs_8/captcp/tsg/seq.data"
			delaytime=$(cat $tsgfile | awk 'END { print $1 }')
			echo -e "$count\t$delaytime" >> $delaydata
			count=$((count+1))
		done

done


		# Write the plot script file for the delays for 2, 4, 6 and 8 nodes.
		pngoutput="delay_all_2.png"

		echo -e "reset" >> $plotfile_delays
		echo -e "" >> $plotfile_delays
		echo -e "set term png" >> $plotfile_delays
		echo -e "" >> $plotfile_delays
		echo -e "set output \"$pngoutput\"" >> $plotfile_delays
		echo -e "set ylabel \"Time (seconds)\"" >> $plotfile_delays
		echo -e "set xlabel \"Kernel Version\"" >> $plotfile_delays
		echo -e "set xrange [1:4]" >> $plotfile_delays
		echo -e "set title \"Delays -- 2 nodes\"" >> $plotfile_delays
		echo -e "" >> $plotfile_delays
		echo -e "set grid" >> $plotfile_delays
		echo -e "set xtics ( \"2.3.6\" 1, \"3.7.0\" 2, \"3.10.0\" 3, \"3.12.0\" 4)" >> $plotfile_delays
		echo -e "set autoscale xfix" >> $plotfile_delays
		echo -e "set offsets 1,1,0,0" >> $plotfile_delays
		echo -e "" >> $plotfile_delays
		echo -e "plot 'delay_bic_2.data' using 1:2 with lines title \"bic\", \\" >> $plotfile_delays
		echo -e "     'delay_cubic_2.data' using 1:2 with lines title \"cubic\", \\" >> $plotfile_delays
		echo -e "     'delay_highspeed_2.data' using 1:2 with lines title \"highspeed\", \\" >> $plotfile_delays
		echo -e "     'delay_htcp_2.data' using 1:2 with lines title \"htcp\", \\" >> $plotfile_delays
		echo -e "     'delay_hybla_2.data' using 1:2 with lines title \"hybla\", \\" >> $plotfile_delays
		echo -e "     'delay_illinois_2.data' using 1:2 with lines title \"illinois\", \\" >> $plotfile_delays
		echo -e "     'delay_lp_2.data' using 1:2 with lines title \"lp\", \\" >> $plotfile_delays
		echo -e "     'delay_reno_2.data' using 1:2 with lines title \"reno\", \\" >> $plotfile_delays
		echo -e "     'delay_scalable_2.data' using 1:2 with lines title \"scalable\", \\" >> $plotfile_delays
		echo -e "     'delay_vegas_2.data' using 1:2 with lines title \"vegas\", \\" >> $plotfile_delays
		echo -e "     'delay_veno_2.data' using 1:2 with lines title \"veno\", \\" >> $plotfile_delays
		echo -e "     'delay_westwood_2.data' using 1:2 with lines title \"westwood\", \\" >> $plotfile_delays
		echo -e "     'delay_yeah_2.data' using 1:2 with lines title \"yeah\"" >> $plotfile_delays

		pngoutput="delay_all_4.png"

		echo -e "" >> $plotfile_delays
		echo -e "set output \"$pngoutput\"" >> $plotfile_delays
		echo -e "set title \"Delays -- 4 nodes\"" >> $plotfile_delays
		echo -e "" >> $plotfile_delays
		echo -e "plot 'delay_bic_4.data' using 1:2 with lines title \"bic\", \\" >> $plotfile_delays
		echo -e "     'delay_cubic_4.data' using 1:2 with lines title \"cubic\", \\" >> $plotfile_delays
		echo -e "     'delay_highspeed_4.data' using 1:2 with lines title \"highspeed\", \\" >> $plotfile_delays
		echo -e "     'delay_htcp_4.data' using 1:2 with lines title \"htcp\", \\" >> $plotfile_delays
		echo -e "     'delay_hybla_4.data' using 1:2 with lines title \"hybla\", \\" >> $plotfile_delays
		echo -e "     'delay_illinois_4.data' using 1:2 with lines title \"illinois\", \\" >> $plotfile_delays
		echo -e "     'delay_lp_4.data' using 1:2 with lines title \"lp\", \\" >> $plotfile_delays
		echo -e "     'delay_reno_4.data' using 1:2 with lines title \"reno\", \\" >> $plotfile_delays
		echo -e "     'delay_scalable_4.data' using 1:2 with lines title \"scalable\", \\" >> $plotfile_delays
		echo -e "     'delay_vegas_4.data' using 1:2 with lines title \"vegas\", \\" >> $plotfile_delays
		echo -e "     'delay_veno_4.data' using 1:2 with lines title \"veno\", \\" >> $plotfile_delays
		echo -e "     'delay_westwood_4.data' using 1:2 with lines title \"westwood\", \\" >> $plotfile_delays
		echo -e "     'delay_yeah_4.data' using 1:2 with lines title \"yeah\"" >> $plotfile_delays

		pngoutput="delay_all_6.png"

		echo -e "" >> $plotfile_delays
		echo -e "set output \"$pngoutput\"" >> $plotfile_delays
		echo -e "set title \"Delays -- 6 nodes\"" >> $plotfile_delays
		echo -e "" >> $plotfile_delays
		echo -e "plot 'delay_bic_6.data' using 1:2 with lines title \"bic\", \\" >> $plotfile_delays
		echo -e "     'delay_cubic_6.data' using 1:2 with lines title \"cubic\", \\" >> $plotfile_delays
		echo -e "     'delay_highspeed_6.data' using 1:2 with lines title \"highspeed\", \\" >> $plotfile_delays
		echo -e "     'delay_htcp_6.data' using 1:2 with lines title \"htcp\", \\" >> $plotfile_delays
		echo -e "     'delay_hybla_6.data' using 1:2 with lines title \"hybla\", \\" >> $plotfile_delays
		echo -e "     'delay_illinois_6.data' using 1:2 with lines title \"illinois\", \\" >> $plotfile_delays
		echo -e "     'delay_lp_6.data' using 1:2 with lines title \"lp\", \\" >> $plotfile_delays
		echo -e "     'delay_reno_6.data' using 1:2 with lines title \"reno\", \\" >> $plotfile_delays
		echo -e "     'delay_scalable_6.data' using 1:2 with lines title \"scalable\", \\" >> $plotfile_delays
		echo -e "     'delay_vegas_6.data' using 1:2 with lines title \"vegas\", \\" >> $plotfile_delays
		echo -e "     'delay_veno_6.data' using 1:2 with lines title \"veno\", \\" >> $plotfile_delays
		echo -e "     'delay_westwood_6.data' using 1:2 with lines title \"westwood\", \\" >> $plotfile_delays
		echo -e "     'delay_yeah_6.data' using 1:2 with lines title \"yeah\"" >> $plotfile_delays

		pngoutput="delay_all_8.png"

		echo -e "" >> $plotfile_delays
		echo -e "set output \"$pngoutput\"" >> $plotfile_delays
		echo -e "set title \"Delays -- 8 nodes\"" >> $plotfile_delays
		echo -e "" >> $plotfile_delays
		echo -e "plot 'delay_bic_8.data' using 1:2 with lines title \"bic\", \\" >> $plotfile_delays
		echo -e "     'delay_cubic_8.data' using 1:2 with lines title \"cubic\", \\" >> $plotfile_delays
		echo -e "     'delay_highspeed_8.data' using 1:2 with lines title \"highspeed\", \\" >> $plotfile_delays
		echo -e "     'delay_htcp_8.data' using 1:2 with lines title \"htcp\", \\" >> $plotfile_delays
		echo -e "     'delay_hybla_8.data' using 1:2 with lines title \"hybla\", \\" >> $plotfile_delays
		echo -e "     'delay_illinois_8.data' using 1:2 with lines title \"illinois\", \\" >> $plotfile_delays
		echo -e "     'delay_lp_8.data' using 1:2 with lines title \"lp\", \\" >> $plotfile_delays
		echo -e "     'delay_reno_8.data' using 1:2 with lines title \"reno\", \\" >> $plotfile_delays
		echo -e "     'delay_scalable_8.data' using 1:2 with lines title \"scalable\", \\" >> $plotfile_delays
		echo -e "     'delay_vegas_8.data' using 1:2 with lines title \"vegas\", \\" >> $plotfile_delays
		echo -e "     'delay_veno_8.data' using 1:2 with lines title \"veno\", \\" >> $plotfile_delays
		echo -e "     'delay_westwood_8.data' using 1:2 with lines title \"westwood\", \\" >> $plotfile_delays
		echo -e "     'delay_yeah_8.data' using 1:2 with lines title \"yeah\"" >> $plotfile_delays

	# Generate all the plots
	echo -e "${orange}[*] ${red}Generating all the plots${nc}"
	find . -type f -name '*.plt' -printf '%h\n' | xargs -i bash -c 'cd $0; gnuplot *.plt; cd -' {}

unset destdir
unset kernels
unset tempres
unset tputfile
unset tputpngfile
unset destputfile
unset destputpngfile
unset avg_tput
unset plotfile_delays
unset delaydata
unset tsgfile
unset delaytime
unset count
unset pngoutput
