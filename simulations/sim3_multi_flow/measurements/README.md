# README #

## What ## 
Once the simulation script have been executed we should have a directory skeleton in the way:

			<dirname_given>/
				sim3_multi_flow/
					bic/
						2.6.36/
							graphs_2/
							graphs_4/
						3.7.0/
							...
						3.10.0/
						3.12.0/
					cubic/
					reno/
					..etc


This simulation was only executed for a zero packet loss case due to the intrinsic added congestion control as a result of several flows competing for a single connection. Scripts have been written for this specific naming & structure, but are easily changeable. A final skeleton structure of directories goes as follows:
			
			measurements/
				sim3_multi_flow/ -> data obtained after executing simulations with sim3.sh
						2.6.36/
							graphs_2/
							graphs_4/
						3.7.0/
							...
						3.10.0/
						3.12.0/				
				sim3_averages/   -> data obtained after executing simulations with sim3_averages.sh
					bic/			
					cubic/
					...
					delay_averages/
					joindata.sh
				extract_data.sh  -> Script to gather all information from data and create plot scripts.

**sim3_multi_flow/** folder keeps all data obtained from executing all scenarios with the **sim3.sh** script. One folder for each tuple (congestion control algorithm, kernel version, number of nodes in client side). Inflight data segments, throughtput and time sequence graphs are measured.
**sim3_averages/** folder gather average measures after 6 executions for throughput and delay execution time for each of the previous combination of congestion control algorithm, kernel version, number of nodes in client side.

			
## How ##
* **extract_data.sh** is the keystone of the structure.
  1. Creates *sim3_data_extracted/* and *delays/* folder.
  2. For each congestion control algorithm, it will gather throughput files for 2 and 4 nodes for all kernel versions and create a gnuplot script to plot them (throughput of one algorithm accross kernel versions).
  3. For each congestion control algorithm, it will gather execution delay times for 2 and 4 nodes for all kernel versions and create a gnuplot script to plot them (delays for all algorithms accross kernel versions for 2 nodes, and other graph for 4 nodes).

* **sim3_averages/** folder contains all mean and standard deviation values for throughput and delay measures after 6 executions of each scenario (congestion control algorithm, kernel version, number of nodes in client side).
* **joindata.sh** script will create the gnuplot scripts to plot either for throughput and delay, the errorplot bars (showing mean value and standard deviation) for each pair of congestion control algorithm, number of nodes in client side. Hence, each graph will show joint values of desired measure (delay or throughput) along all four different kernel versions.
