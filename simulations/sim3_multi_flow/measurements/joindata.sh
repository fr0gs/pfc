#!/bin/bash

# List of different kernel versions
kernels=( "2.6.36" "3.7.0" "3.10.0" "3.12.0" )

# Get current directory
curdir=$(pwd)

rm -rf delay_averages
mkdir delay_averages

plot_delay_avg="plot_delay_join.plt"
touch $plot_delay_avg

pngoutput="delay_all_avg_2.png"

echo -e "reset" >> $plot_delay_avg
echo -e "" >> $plot_delay_avg
echo -e "set term png" >> $plot_delay_avg
echo -e "" >> $plot_delay_avg
echo -e "set output \"$pngoutput\"" >> $plot_delay_avg
echo -e "set ylabel \"Time (seconds)\"" >> $plot_delay_avg
echo -e "set xlabel \"Kernel Version\"" >> $plot_delay_avg
echo -e "set xrange [1:4]" >> $plot_delay_avg
echo -e "set title \"Average Delays -- 6 runs -- 2 nodes\"" >> $plot_delay_avg
echo -e "" >> $plot_delay_avg
echo -e "set grid" >> $plot_delay_avg
echo -e "set xtics ( \"2.3.6\" 1, \"3.7.0\" 2, \"3.10.0\" 3, \"3.12.0\" 4)" >> $plot_delay_avg
echo -e "set autoscale xfix" >> $plot_delay_avg
echo -e "set offsets 1,1,0,0" >> $plot_delay_avg
echo -e "" >> $plot_delay_avg
echo -e "plot 'delay_bic_join_2.data' using 1:2 with lines title \"bic\", \\" >> $plot_delay_avg
echo -e "     'delay_cubic_join_2.data' using 1:2 with lines title \"cubic\", \\" >> $plot_delay_avg
echo -e "     'delay_highspeed_join_2.data' using 1:2 with lines title \"highspeed\", \\" >> $plot_delay_avg
echo -e "     'delay_htcp_join_2.data' using 1:2 with lines title \"htcp\", \\" >> $plot_delay_avg
echo -e "     'delay_hybla_join_2.data' using 1:2 with lines title \"hybla\", \\" >> $plot_delay_avg
echo -e "     'delay_illinois_join_2.data' using 1:2 with lines title \"illinois\", \\" >> $plot_delay_avg
echo -e "     'delay_lp_join_2.data' using 1:2 with lines title \"lp\", \\" >> $plot_delay_avg
echo -e "     'delay_reno_join_2.data' using 1:2 with lines title \"reno\", \\" >> $plot_delay_avg
echo -e "     'delay_scalable_join_2.data' using 1:2 with lines title \"scalable\", \\" >> $plot_delay_avg
echo -e "     'delay_vegas_join_2.data' using 1:2 with lines title \"vegas\", \\" >> $plot_delay_avg
echo -e "     'delay_veno_join_2.data' using 1:2 with lines title \"veno\", \\" >> $plot_delay_avg
echo -e "     'delay_westwood_join_2.data' using 1:2 with lines title \"westwood\", \\" >> $plot_delay_avg
echo -e "     'delay_yeah_join_2.data' using 1:2 with lines title \"yeah\"" >> $plot_delay_avg

pngoutput="delay_all_avg_4.png"
echo -e "" >> $plot_delay_avg
echo -e "set output \"$pngoutput\"" >> $plot_delay_avg
echo -e "set title \"Average Delays -- 6 runs -- 4 nodes\"" >> $plot_delay_avg
echo -e "" >> $plot_delay_avg
echo -e "     'delay_cubic_join_4.data' using 1:2 with lines title \"cubic\", \\" >> $plot_delay_avg
echo -e "     'delay_htcp_join_4.data' using 1:2 with lines title \"htcp\", \\" >> $plot_delay_avg
echo -e "     'delay_illinois_join_4.data' using 1:2 with lines title \"illinois\", \\" >> $plot_delay_avg
echo -e "     'delay_lp_join_4.data' using 1:2 with lines title \"lp\", \\" >> $plot_delay_avg
echo -e "     'delay_veno_join_4.data' using 1:2 with lines title \"veno\", \\" >> $plot_delay_avg
echo -e "     'delay_yeah_join_4.data' using 1:2 with lines title \"yeah\"" >> $plot_delay_avg


pngoutput="delay_all_avg_6.png"
echo -e "" >> $plot_delay_avg
echo -e "set output \"$pngoutput\"" >> $plot_delay_avg
echo -e "set title \"Average Delays -- 6 runs -- 6 nodes\"" >> $plot_delay_avg
echo -e "" >> $plot_delay_avg
echo -e "     'delay_cubic_join_6.data' using 1:2 with lines title \"cubic\", \\" >> $plot_delay_avg
echo -e "     'delay_htcp_join_6.data' using 1:2 with lines title \"htcp\", \\" >> $plot_delay_avg
echo -e "     'delay_illinois_join_6.data' using 1:2 with lines title \"illinois\", \\" >> $plot_delay_avg
echo -e "     'delay_lp_join_6.data' using 1:2 with lines title \"lp\", \\" >> $plot_delay_avg
echo -e "     'delay_veno_join_6.data' using 1:2 with lines title \"veno\", \\" >> $plot_delay_avg
echo -e "     'delay_yeah_join_6.data' using 1:2 with lines title \"yeah\"" >> $plot_delay_avg


pngoutput="delay_all_avg_8.png"
echo -e "" >> $plot_delay_avg
echo -e "set output \"$pngoutput\"" >> $plot_delay_avg
echo -e "set title \"Average Delays -- 6 runs -- 8 nodes\"" >> $plot_delay_avg
echo -e "" >> $plot_delay_avg
echo -e "     'delay_cubic_join_8.data' using 1:2 with lines title \"cubic\", \\" >> $plot_delay_avg
echo -e "     'delay_htcp_join_8.data' using 1:2 with lines title \"htcp\", \\" >> $plot_delay_avg
echo -e "     'delay_illinois_join_8.data' using 1:2 with lines title \"illinois\", \\" >> $plot_delay_avg
echo -e "     'delay_lp_join_8.data' using 1:2 with lines title \"lp\", \\" >> $plot_delay_avg
echo -e "     'delay_veno_join_8.data' using 1:2 with lines title \"veno\", \\" >> $plot_delay_avg
echo -e "     'delay_yeah_join_8.data' using 1:2 with lines title \"yeah\"" >> $plot_delay_avg


cp $plot_delay_avg "delay_averages/"


for alg in `ls`
do

	if [[ -d $alg && $alg != "delay_averages" ]]; then
		cd $alg

		delay_join2="delay_"$alg"_join_2.data"
		delay_join2_avg="delay_"$alg"_join_avg_2.data"

		delay_join4="delay_"$alg"_join_4.data"
		delay_join4_avg="delay_"$alg"_join_avg_4.data"

		delay_join6="delay_"$alg"_join_6.data"
		delay_join6_avg="delay_"$alg"_join_avg_6.data"

		delay_join8="delay_"$alg"_join_8.data"
		delay_join8_avg="delay_"$alg"_join_avg_8.data"

		tput_join2="tput_"$alg"_join_2.data"
		tput_join4="tput_"$alg"_join_4.data"
		tput_join6="tput_"$alg"_join_6.data"
		tput_join8="tput_"$alg"_join_8.data"

		plotfile="plot_"$alg".plt"

		rm -f $plotfile

		rm -f $delay_join2
		rm -f $delay_join4
		rm -f $delay_join6
		rm -f $delay_join8

		rm -f $delay_join2_avg
		rm -f $delay_join4_avg
		rm -f $delay_join6_avg
		rm -f $delay_join8_avg

		rm -f $tput_join2
		rm -f $tput_join4
		rm -f $tput_join6
		rm -f $tput_join8

		touch $delay_join2
		touch $delay_join2_avg
		
		touch $delay_join4
		touch $delay_join4_avg

		touch $delay_join6
		touch $delay_join6_avg

		touch $delay_join8
		touch $delay_join8_avg

		touch $tput_join2
		touch $tput_join4
		touch $tput_join6
		touch $tput_join8

		# delay_join2/4/6/8 consist only of 4 entries, each one corresponding to a single kernel version and it's average delay
		# delay_join_avg_2/4/6/8 is the joint of all entries as for the throughput, to get it's error bar plots

		count=1
		for kernv in ${kernels[@]}
		do	
			avg_val=$(cat "delay_"$alg"_"$kernv"_2.data" | awk '{ sum += $1; n++ } END { if (n > 0) print sum / n; }')
			echo -e "$count\t$avg_val" >> $delay_join2
	
			avg_val=$(cat "delay_"$alg"_"$kernv"_4.data" | awk '{ sum += $1; n++ } END { if (n > 0) print sum / n; }')
			echo -e "$count\t$avg_val" >> $delay_join4

			avg_val=$(cat "delay_"$alg"_"$kernv"_6.data" | awk '{ sum += $1; n++ } END { if (n > 0) print sum / n; }')
			echo -e "$count\t$avg_val" >> $delay_join6

			avg_val=$(cat "delay_"$alg"_"$kernv"_8.data" | awk '{ sum += $1; n++ } END { if (n > 0) print sum / n; }')
			echo -e "$count\t$avg_val" >> $delay_join8

			cat "delay_"$alg"_"$kernv"_2.data" | awk -v c=$((count)) '{ print $1, "\t", c}' >> $delay_join2_avg
			echo "" >> $delay_join2_avg
			echo "" >> $delay_join2_avg

			cat "delay_"$alg"_"$kernv"_4.data" | awk -v c=$((count)) '{ print $1, "\t", c}' >> $delay_join4_avg
			echo "" >> $delay_join4_avg
			echo "" >> $delay_join4_avg

			cat "delay_"$alg"_"$kernv"_6.data" | awk -v c=$((count)) '{ print $1, "\t", c}' >> $delay_join6_avg
			echo "" >> $delay_join6_avg
			echo "" >> $delay_join6_avg

			cat "delay_"$alg"_"$kernv"_8.data" | awk -v c=$((count)) '{ print $1, "\t", c}' >> $delay_join8_avg
			echo "" >> $delay_join8_avg
			echo "" >> $delay_join8_avg


			cat "tput_"$alg"_"$kernv"_2.data" | awk -v c=$((count)) '{ print $1, "\t", c}' >> $tput_join2
			echo "" >> $tput_join2	
			echo "" >> $tput_join2
	
			cat "tput_"$alg"_"$kernv"_4.data" | awk -v c=$((count)) '{ print $1, "\t", c}' >> $tput_join4
			echo "" >> $tput_join4	
			echo "" >> $tput_join4

			cat "tput_"$alg"_"$kernv"_6.data" | awk -v c=$((count)) '{ print $1, "\t", c}' >> $tput_join6
			echo "" >> $tput_join6
			echo "" >> $tput_join6

			cat "tput_"$alg"_"$kernv"_8.data" | awk -v c=$((count)) '{ print $1, "\t", c}' >> $tput_join8
			echo "" >> $tput_join8	
			echo "" >> $tput_join8


			count=$((count+1))
		done
		
		# Trim the last two blank lines of the data file to make it readable
		cat $tput_join2 | head -n -2 > aux.data
		mv aux.data $tput_join2

		cat $tput_join4 | head -n -2 > aux.data
		mv aux.data $tput_join4

		cat $tput_join6 | head -n -2 > aux.data
		mv aux.data $tput_join6

		cat $tput_join8 | head -n -2 > aux.data
		mv aux.data $tput_join8

		cat $delay_join2_avg | head -n -2 > aux.data
		mv aux.data $delay_join2_avg

		cat $delay_join4_avg | head -n -2 > aux.data
		mv aux.data $delay_join4_avg

		cat $delay_join6_avg | head -n -2 > aux.data
		mv aux.data $delay_join6_avg

		cat $delay_join8_avg | head -n -2 > aux.data
		mv aux.data $delay_join8_avg


		# Plotting the tput errorbar plot for 2, 4, 6 and 8 nodes
		echo "reset" >> $plotfile
		echo "system('rm -f tput_"$alg"_join_2.png')" >> $plotfile

		echo "" >> $plotfile
		echo "if (!exists(\"FILEIN\")) FILEIN='$tput_join2'" >> $plotfile
		echo "set term png" >> $plotfile
		echo "set output \"tput_"$alg"_join_2.png\"" >> $plotfile
		echo "set ylabel \"Mean + STDDEV\"" >> $plotfile
		echo "set xlabel \"Kernel Version\"" >> $plotfile
		echo "set xrange [1:4]" >> $plotfile
		echo "set title \"Average throughput (Kb/s) - $alg - 2 nodes\"" >> $plotfile
		echo "" >> $plotfile
		echo "set grid" >> $plotfile
		echo "" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered_tput_2.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "set xtics ( \"2.3.6\" 1, \"3.7.0\" 2, \"3.10.0\" 3, \"3.12.0\" 4)" >> $plotfile
		echo "set autoscale xfix" >> $plotfile
		echo "set offsets 1,1,0,0" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered_tput_2.data' using 1:2:3 with yerrorbars" >> $plotfile
		
		# 4-node part
		echo "" >> $plotfile
		echo "" >> $plotfile
		echo "system('rm -f tput_"$alg"_join_4.png')" >> $plotfile
		echo "system('rm -f data_gathered_tput_4.data')" >> $plotfile
		echo "set output \"tput_"$alg"_join_4.png\"" >> $plotfile
		echo "set title \"Average throughput (Kb/s) - $alg - 4 nodes\"" >> $plotfile
		echo "" >> $plotfile
		echo "FILEIN='$tput_join4'" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered_tput_4.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered_tput_4.data' using 1:2:3 with yerrorbars" >> $plotfile

		# 6-node part
		echo "" >> $plotfile
		echo "" >> $plotfile
		echo "system('rm -f tput_"$alg"_join_6.png')" >> $plotfile
		echo "system('rm -f data_gathered_tput_6.data')" >> $plotfile
		echo "set output \"tput_"$alg"_join_6.png\"" >> $plotfile
		echo "set title \"Average throughput (Kb/s) - $alg - 6 nodes\"" >> $plotfile
		echo "" >> $plotfile
		echo "FILEIN='$tput_join6'" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered_tput_6.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered_tput_6.data' using 1:2:3 with yerrorbars" >> $plotfile

		# 8-node part
		echo "" >> $plotfile
		echo "" >> $plotfile
		echo "system('rm -f tput_"$alg"_join_8.png')" >> $plotfile
		echo "system('rm -f data_gathered_tput_8.data')" >> $plotfile
		echo "set output \"tput_"$alg"_join_8.png\"" >> $plotfile
		echo "set title \"Average throughput (Kb/s) - $alg - 8 nodes\"" >> $plotfile
		echo "" >> $plotfile
		echo "FILEIN='$tput_join8'" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered_tput_8.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered_tput_8.data' using 1:2:3 with yerrorbars" >> $plotfile

		

		# Plotting the delay errorbar plot for 2 and 4 nodes
		echo "" >> $plotfile
		echo "" >> $plotfile
		echo "set output \"delay_"$alg"_join_avg_2.png\"" >> $plotfile
		echo "set title \"Average Delay (seconds)- $alg - 2 nodes\"" >> $plotfile
		echo "set ylabel \"Mean + STDDEV\"" >> $plotfile
		echo "" >> $plotfile
		echo "FILEIN='$delay_join2_avg'" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered_2.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered_2.data' using 1:2:3 with yerrorbars" >> $plotfile

		# 4-node part
		echo "" >> $plotfile
		echo "" >> $plotfile
		echo "set output \"delay_"$alg"_join_avg_4.png\"" >> $plotfile
		echo "set title \"Average Delay (seconds) - $alg - 4 nodes\"" >> $plotfile
		echo "" >> $plotfile
		echo "FILEIN='$delay_join4_avg'" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered_4.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered_4.data' using 1:2:3 with yerrorbars" >> $plotfile

		# 6-node part
		echo "" >> $plotfile
		echo "" >> $plotfile
		echo "set output \"delay_"$alg"_join_avg_6.png\"" >> $plotfile
		echo "set title \"Average Delay (seconds) - $alg - 6 nodes\"" >> $plotfile
		echo "" >> $plotfile
		echo "FILEIN='$delay_join6_avg'" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered_6.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered_6.data' using 1:2:3 with yerrorbars" >> $plotfile

		# 8-node part
		echo "" >> $plotfile
		echo "" >> $plotfile
		echo "set output \"delay_"$alg"_join_avg_8.png\"" >> $plotfile
		echo "set title \"Average Delay (seconds) - $alg - 8 nodes\"" >> $plotfile
		echo "" >> $plotfile
		echo "FILEIN='$delay_join8_avg'" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered_8.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered_8.data' using 1:2:3 with yerrorbars" >> $plotfile


		cp $delay_join2 $curdir"/delay_averages"
		cp $delay_join4 $curdir"/delay_averages"
		cp $delay_join6 $curdir"/delay_averages"
		cp $delay_join8 $curdir"/delay_averages"
		#cp $delay_join2_avg $curdir"/delay_averages"
		#cp $delay_join4_avg $curdir"/delay_averages"

		unset delay_join2
		unset delay_join4
		unset delay_join6
		unset delay_join8
		unset tput_join2
		unset tput_join4
		unset tput_join6
		unset tput_join8

		cd - &>/dev/null
	fi
done


rm -f $plot_delay_avg

