dest=$1
alg=$2
nodes=$3

declare yellow='\033[1;33m'
declare cyan='\033[1;36m'
declare green='\033[1;32m'
declare nc='\033[0m'


if [ "$#" -ne 3 ]; then
	echo "Usage: $ ./sim3_averages.sh <dest_dir> <tcp_congestion_control> <nodes>"
	exit -1
fi

direc=$PFCDIR"measures/"$dest"/"
if [ ! -d "$direc" ]; then
  mkdir $direc
fi

mkdir $direc


direc=$direc"/"$alg
if [ ! -d "$direc" ]; then
  mkdir $direc
fi


kernels=( "2.6" "3.7" "3.10" "3.12" ) #"3.14" )

echo -e "[+] ${green} Number of nodes: ${nc} $nodes"

for kernelver in ${kernels[@]}
do
	echo -e "[+] ${green} Algorithm: ${nc} $alg"
	echo -e "[+] ${green} Kernel version: ${nc} $kernelver"
	
	cdmysc $kernelver
	cd sim3_multi_flow/
	
	mkdir temp

	declare destfichtput="tput_"$alg"_"$kernelver
	declare destfichdelay="delay_"$alg"_"$kernelver
	
	if [ $kernelver == "2.6" ];
	then
		destfichtput=$destfichtput".36_"$nodes".data"
		destfichdelay=$destfichdelay".36_"$nodes".data"
	else
		destfichtput=$destfichtput".0_"$nodes".data"
		destfichdelay=$destfichdelay".0_"$nodes".data"
	fi


	rm -f $destfichtput &>/dev/null
	rm -f $destfichdelay &>/dev/null

	touch $destfichtput
	touch $destfichdelay


	for i in `seq 1 6`
	do
		trick=$i
	
		# This is a crappy trick to avoid the simulation to segfault with 
		# tricky combinations kernel version / run number
		if [[ $kernelver == "3.7" && $i == 2 ]]; then
				trick=$(shuf -i 1-100 -n 1)
		fi

		if [[ $kernel == "3.10" && $i == 1 ]]; then
				trick=$(shuf -i 1-100 -n 1)
		fi
		
		if [[ $kernel == "3.10" && $i == 2 ]]; then
				trick=$(shuf -i 1-100 -n 1)
		fi

		#Execute the simulation
		wafdce --run "sim3_multi_flow --nodes=$nodes --run=$trick --error=0 --algorithm=$alg --bc=1024K --bbg=4096K"
		
		echo -e "${yellow}[->] ${cyan} tput graph for iteration: ${nc} $i"
		captcp throughput -f 1.1 -u kilobyte -i -o temp/ csma-0-0.pcap &>/dev/null 
		tpinf-stat temp/throughput.data | awk -v var=$i 'END { print $3"\t"var }' >> $destfichtput

		rm -f temp/*

		echo -e "${yellow}[->] ${cyan} tsg graph for iteration: ${nc} $i"
		captcp timesequence -f 1.1 -o temp/ -i -e csma-0-0.pcap &>/dev/null
		tsg=$(cat temp/seq.data | awk 'END { print $1 }')
		echo $tsg >> $destfichdelay
	done
	
	echo -e "${green}************************************************${nc}"

	rm -rf temp/

	mv $destfichtput $direc
	mv $destfichdelay $direc

done

unset direc
unset destfichtput
unset destfichowin
