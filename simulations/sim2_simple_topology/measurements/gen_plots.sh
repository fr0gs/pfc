#!/bin/bash

# As I know there is only one .plt per directory,
# it just executes all plots and copies the graphics into the current dir.


# This line finds all the "joinbykernel.sh" scripts in each kernel directory (in by_kernel) and executes it.
find by_kernel/ -type f -name 'joinbykernel.sh' -printf '%h\n' | xargs -i bash -c 'cd "$0"; bash ./joinbykernel.sh; cd -' {}

# This line find all plot script files (.plt) and executes gnuplot to them
find . -name '*.plt' -printf "%h\n" | xargs -i bash -c 'cd "$0"; rm *.png; gnuplot *.plt ; cd -' {}


rm -rf graphics/
mkdir -p graphics
mkdir graphics/tput
mkdir graphics/owin

# This line finds all tput png files in this dir and subdirs and copies them to graphics/ dir
find . -name '*tput*.png' -printf '%p\n' | xargs -i bash -c 'cp -rf $0 graphics/tput' {} 

# This line finds all owin png files in this dir and subdirs and copies them to graphics/ dir
find . -name '*owin*.png' -printf '%p\n' | xargs -i bash -c 'cp -rf $0 graphics/owin' {} 

