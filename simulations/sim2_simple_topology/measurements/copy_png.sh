#!/bin/bash

find . -type f -name '*.png' -printf '%p\n' | xargs -i bash -c 'cp -rf $0 all_graphs/' {} &>/dev/null
