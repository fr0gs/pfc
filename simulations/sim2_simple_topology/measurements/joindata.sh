#!/bin/bash

#Join all tput logs into a single one
#This script must be run from avg_measures_X folder.


declare hey=0
declare ploss=$(pwd | awk -F'/' '{ print $NF }' | awk -F'_' '{ print $NF }') # Get the rate number.

for alg in `ls`
do
	
	if [[ -d $alg && $alg != "graphics" && $alg != "by_kernel" ]]; then
		# Change to the algorithms dir
		cd $alg 
		
		# Remove previous plot file and the auxiliary data file.
		declare plotfile="plot_"$ploss"_"$alg"_join.plt" # Something like plot_0_bic_join.plt	
		rm $plotfile


		echo "En el directorio : $alg/ por dios que no pete nada"

		# Declare the name of the tput joint data file
		declare outfile="tput_"$ploss"_"$alg"_join.data" # Something like tput_0_bic_join.data
		rm $outfile

		# For each tput data file in the dir I read it and stit the output +2 blank lines into the
		# final data file
		for ho in `ls | grep tput`
		do
			if [[ $ho != *.png ]]; then
				cat $ho | awk -v count=$hey '{ print $1"\t" count }' >> $outfile
				echo "" >> $outfile
				echo "" >> $outfile
				hey=$((hey+1))
			fi 
		done
	
		hey=0
		
		# Trim the last two blank lines of the data file to make it readable
		cat $outfile | head -n -4 > aux.data
		mv aux.data $outfile

	
		#Generate a gnuplot file for this script
		echo "reset" >> $plotfile
		echo "system('rm -f tput_"$ploss"_"$alg".png')" >> $plotfile
		echo "system('rm -f data_gathered.data')" >> $plotfile
		echo "" >> $plotfile
		echo "if (!exists(\"FILEIN\")) FILEIN='$outfile'" >> $plotfile
		echo "set term png" >> $plotfile
		echo "set output \"tput_"$ploss"_"$alg".png\"" >> $plotfile
		echo "set ylabel \"Mean + STDDEV\"" >> $plotfile
		echo "set xlabel \"Kernel Version\"" >> $plotfile
		echo "set xrange [0:4]" >> $plotfile
		echo "set title \"Average throughput\"" >> $plotfile
		echo "" >> $plotfile
		echo "set grid" >> $plotfile
		echo "" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "set xtics ( \"2.3.6\" 0, \"3.7.0\" 1, \"3.10.0\" 2, \"3.12.0\" 3)" >> $plotfile
		echo "set autoscale xfix" >> $plotfile
		echo "set offsets 1,1,0,0" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered.data' using 1:2:3 with yerrorbars" >> $plotfile

		
		# Declare the name of the owin joint data file
		declare outfile="owin_"$ploss"_"$alg"_join.data" # Something like tput_0_bic_join.data
		rm $outfile
		
		
		# I repeat the same process I did with the tput data file but with the owin files.
		for ho in `ls | grep owin`
		do
			# This silly check is because I do my programming fast and with no clues, if I don't make it
			# and there is a png file with the string tput for name, I cat the png file just after the whole
			# data.
			if [[ $ho != *.png ]]; then
				cat $ho | awk -v count=$hey '{ print $1"\t" count }' >> $outfile
				echo "" >> $outfile
				echo "" >> $outfile
				hey=$((hey+1))
			fi 
		done
	
		hey=0
		
		# Trim the last two blank lines of the data file to make it readable
		cat $outfile | head -n -4 > aux.data
		mv aux.data $outfile
			
		
		# Now I add the lines to the plotfile needed to plot the owin data too
		echo "" >> $plotfile
		echo "" >> $plotfile	
		echo "system('rm -f owin_"$ploss"_"$alg".png')" >> $plotfile
		echo "system('rm -f data_gathered.data')" >> $plotfile
		echo "" >> $plotfile
		echo "FILEIN='$outfile'" >> $plotfile
		echo "set term png" >> $plotfile
		echo "set output \"owin_"$ploss"_"$alg".png\"" >> $plotfile
		echo "set title \"Average Owin (Inflight Data Segments)\"" >> $plotfile
		echo "" >> $plotfile
		echo "set grid" >> $plotfile
		echo "" >> $plotfile
		echo "stats FILEIN using 0 nooutput" >> $plotfile
		echo "" >> $plotfile
		echo "set print 'data_gathered.data'" >> $plotfile
		echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
		echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
		echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
		echo "}" >> $plotfile
		echo "" >> $plotfile
		echo "plot 'data_gathered.data' using 1:2:3 with yerrorbars" >> $plotfile

		unset outfile
		unset plotfile

		cd - &>/dev/null
	fi

done


unset hey
unset ploss



