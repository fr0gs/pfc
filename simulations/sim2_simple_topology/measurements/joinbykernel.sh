#!/bin/bash

#Join all tput logs into a single one
#This script must be run from avg_measures_X folder.

kernelver=$(pwd | awk -F'/' '{ print $NF }')

declare outfile="tput_"$kernelver"_join.data" # Something like tput_0_bic_join.data
declare plotfile="plot_"$kernelver"_join.plt" # Something like plot_0_bic_join.plt

rm $outfile
rm $plotfile

declare hey=0

#For each tput data file in the dir
for ho in `ls | grep tput`
do
	if [[ $ho != *.png ]]; then
		echo $ho
		cat $ho | awk -v count=$hey '{ print $1"\t" count }' >> $outfile
		echo "" >> $outfile
		echo "" >> $outfile
		hey=$((hey+1))
	fi 
done
	
cat $outfile | head -n -2 > aux.dat
mv aux.dat $outfile

hey=0

	
#Generate a gnuplot file for this script
echo "reset" >> $plotfile
echo "system('rm -f tput_\"$kernelver\".png')" >> $plotfile
echo "system('rm -f data_gathered.data')" >> $plotfile
echo "" >> $plotfile
echo "if (!exists(\"FILEIN\")) FILEIN='$outfile'" >> $plotfile
echo "set term png" >> $plotfile
echo "set output \"tput_"$kernelver".png\"" >> $plotfile
echo "set ylabel \"Mean + STDDEV\"" >> $plotfile
echo "set xlabel \"Congestion Control Algorithm\"" >> $plotfile
echo "set xrange [0:12]" >> $plotfile
echo "set title \"Average throughput - Kernel: $kernelver\"" >> $plotfile
echo "" >> $plotfile
echo "set grid" >> $plotfile
echo "" >> $plotfile
echo "stats FILEIN using 0 nooutput" >> $plotfile
echo "" >> $plotfile
echo "set print 'data_gathered.data'" >> $plotfile
echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
echo "}" >> $plotfile
echo "" >> $plotfile	
echo "set xtics ( \"bc\" 0, \"cbc\" 1, \"hgsp\" 2, \"htcp\" 3, \"hyb\" 4, \"ill\" 5, \"lp\" 6, \"ren\" 7, \"sclbl\" 8, \"veg\" 9, \"ven\" 10, \"wstw\" 11, \"yeh\" 12 )" >> $plotfile
echo "set autoscale xfix" >> $plotfile
echo "set offsets 1,1,0,0" >> $plotfile
echo "" >> $plotfile
echo "plot 'data_gathered.data' using 1:2:3 with yerrorbars" >> $plotfile



outfile="owin_"$kernelver"_join.data" # Something like tput_0_bic_join.data
rm $outfile

# I repeat the same process I did with the tput data file but with the owin files.
for ho in `ls | grep owin`
do
	# This silly check is because I do my programming fast and with no clues, if I don't make it
	# and there is a png file with the string tput for name, I cat the png file just after the whole
	# data.
	if [[ $ho != *.png ]]; then
		cat $ho | awk -v count=$hey '{ print $1"\t" count }' >> $outfile
		echo "" >> $outfile
		echo "" >> $outfile
		hey=$((hey+1))
	fi 
done

hey=0
		
# Trim the last two blank lines of the data file to make it readable
cat $outfile | head -n -4 > aux.data
mv aux.data $outfile

echo "" >> $plotfile
echo "" >> $plotfile	
echo "system('rm -f owin_"$ploss"_"$alg".png')" >> $plotfile
echo "system('rm -f data_gathered.data')" >> $plotfile
echo "" >> $plotfile
echo "FILEIN='$outfile'" >> $plotfile
echo "set output \"owin_"$kernelver".png\"" >> $plotfile
echo "set title \"Average Owin (Inflight Data Segments)\"" >> $plotfile
echo "" >> $plotfile
echo "set grid" >> $plotfile
echo "" >> $plotfile
echo "stats FILEIN using 0 nooutput" >> $plotfile
echo "" >> $plotfile
echo "set print 'data_gathered.data'" >> $plotfile
echo "do for [i=0:STATS_blocks-1] {" >> $plotfile
echo -e "\tstats FILEIN using 2:1 index i nooutput" >> $plotfile
echo -e "\tprint sprintf(\"%e %e %e\", STATS_max_x, STATS_mean_y, STATS_stddev_y)" >> $plotfile
echo "}" >> $plotfile
echo "" >> $plotfile
echo "plot 'data_gathered.data' using 1:2:3 with yerrorbars" >> $plotfile


unset outfile
unset plotfile
unset hey
unset ploss

cd - &>/dev/null



