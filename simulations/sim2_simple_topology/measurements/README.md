# README #

## What ## 
Once the simulation script have been executed we should have a directory skeleton in the way:

			<dirname_given>/
				sim2_simple_topology/
					bic/
					cubic/
					reno/
					..etc

With this in mind, imagine a scenario where the **sim2.sh** has been executed for every common congestion control algorithm available in all kernel versions, changing in each one the rate of packet loss. Scripts have been written for this specific naming & structure, but are easily changeable. In the end, a skeleton structure of directories goes as follows. Imagine that **proyecto_measurements_X_PLOSS** is the common name for all, being X the packet loss rate. I'll be explaining each script sepparately:
			
			measurements/
				proyecto_measurements_0_PLOSS/
					sim2_simple_topology/
						bic/
						cubic/
						reno/
						...
					avg_measures_0/	
						by_kernel/
							2.6.36/
								..data..
								joinbykernel.sh
						bic/
						cubic/
						reno/
						...
						gen_plots.sh
						joindata.sh
						bykernel.bash
				proyecto_measurements_1_PLOSS/
				...
				copy_png.sh
				copystuff.sh
				gen_graphs.sh
				get_delays.sh		

The **avg_measures_0** folder correspond to an execution of the **sim2_averages.sh** script for all congestion control algorithms in the zero packet loss scenario. It is put inside the **proyecto_measurements_0_PLOSS** out of comfort.
The **by_kernel/** folder inside **avg_measures_0** holds average throughput and owin measures grouped by kernel version instead of congestion control algoritm.

			
## How ##
* **copy_png.sh**, **copystuff.sh** and **gen_graphs.sh** are just silly scripts made to ease my life when gathering and clasifying all the
data files.
* **get_delays.sh**: this script will gather the delay times of a given algorithm and a certain packet loss, for each kernel version, in a single file. Hence, there will be one file for each algorithm with four rows, each one corresponding to a different kernel version executed in ascending order. An example of file is for example:

			$ cat delay_0_bic.data

			1	822.997999  -> kernel 2.6.36
			2	837.909250	-> kernel 3.7.0
			3	837.166011	-> kernel 3.10.0
			4	864.950087	-> kernel 3.12.0

All files for the same packet loss value are grouped in the same folder, and then finally, **get_delays.sh** will create a specific gnuplot script in the same folder to collectively plot all algorithm delays for each kernel version. Plotting is done with lines, although it is not the most accurate way to do it, it can serve as an initial approach.

* Inside **avg_measures_X/** folders:
  * **joindata.sh**: joins all tput logs into a single one, and generates a gnuplot script to create errorbar plots.
  * **joinbykernel.sh**: joins all tput logs into a single one, but instead for all kernel versions and one algorithm, this one groups all algorithm for one kernel version.
  * **bykernel.sh**: simple script to help me gather throughput data files and group them by kernel version.
  * **gen_plots.sh**: script that automates the task of generating the plots and copy png files to a dir.


