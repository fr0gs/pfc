#!/bin/bash

declare dir="delays"

rm -rf $dir
mkdir $dir

for i in `ls | grep proyecto_measurements`
do
 	num=$(echo $i | awk -F'_' '{ print $3 }')	
	mkdir $dir"/pl"$num
	declare route=$i"/sim2_simple_topology"

	echo "[+] Enter in:  $route"
	# This will fail as it already exists after the first loop, but I don't care.
	for alg in `ls $route`
	do
		echo -e "\t** For algorithm: $alg"
		declare r=$route"/"$alg	
		declare fich_alg=$dir"/pl"$num"/delay_"$num"_"$alg".data"
		declare plot_aux=$dir"/pl"$num"/plot_"$num"_delay.plt"
		mkdir $dir"/pl"$num
		touch $fich_alg
		touch $plot_aux
		
		declare count=1
		for kernel in `ls $r`
		do
			echo -e "\t\t++ Kernel version: $kernel"
			declare heh=$(cat $r"/"$kernel"/graphs/captcp/tsg/seq.data" | awk 'END { print $1 }')
			echo -e "$count\t$heh" >> $fich_alg
			count=$((count+1))
		done
		unset r
	done

		declare pngoutput="delay_"$num"_all.png"

	# Create a gnuplot for each packet loss.
		echo -e "reset" >> $plot_aux
		echo -e "" >> $plot_aux
		echo -e "set term png" >> $plot_aux
		echo -e "" >> $plot_aux
		echo -e "set output \"$pngoutput\"" >> $plot_aux
		echo -e "set ylabel \"Time (seconds)\"" >> $plot_aux
		echo -e "set xlabel \"Kernel Version\"" >> $plot_aux
		echo -e "set xrange [1:4]" >> $plot_aux
		echo -e "set title \"Delay "$num"% Packet Loss\"" >> $plot_aux
		echo -e "" >> $plot_aux
		echo -e "set grid" >> $plot_aux
		echo -e "set xtics ( \"2.3.6\" 1, \"3.7.0\" 2, \"3.10.0\" 3, \"3.12.0\" 4)" >> $plot_aux
		echo -e "set autoscale xfix" >> $plot_aux
		echo -e "set offsets 1,1,0,0" >> $plot_aux
		echo -e "" >> $plot_aux
		echo -e "plot 'delay_"$num"_bic.data' using 1:2 with lines title \"bic\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_cubic.data' using 1:2 with lines title \"cubic\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_highspeed.data' using 1:2 with lines title \"highspeed\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_htcp.data' using 1:2 with lines title \"htcp\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_hybla.data' using 1:2 with lines title \"hybla\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_illinois.data' using 1:2 with lines title \"illinois\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_lp.data' using 1:2 with lines title \"lp\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_reno.data' using 1:2 with lines title \"reno\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_scalable.data' using 1:2 with lines title \"scalable\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_vegas.data' using 1:2 with lines title \"vegas\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_veno.data' using 1:2 with lines title \"veno\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_westwood.data' using 1:2 with lines title \"westwood\", \\" >> $plot_aux
		echo -e "     'delay_"$num"_yeah.data' using 1:2 with lines title \"yeah\"" >> $plot_aux
done
