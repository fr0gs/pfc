#!/bin/bash

declare dir=$1

find $dir -type f -name 'Makefile' -printf '%h\n' | xargs -i bash -c 'cd "$0"; make png; cd -' {}


