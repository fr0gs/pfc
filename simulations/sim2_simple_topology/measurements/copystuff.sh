#!/bin/bash

alg=$1
fold=$2

#/graphs/captcp/owin/inflight.data
#/graphs/captcp/tput/throughput.data

dir='../../sim2_simple_topology/'$alg

echo $dir

for i in `ls $dir`
do
		if [[ $fold == "tput"  ]];
		then
			jk=$dir"/"$i"/graphs/captcp/tput/throughput.data"
		fi
		if [[ $fold == "owin"  ]];
		then
			jk=$dir"/"$i"/graphs/captcp/owin/inflight.data"
		fi
		echo $jk
		cp $jk $alg"_"$fold"/"$i".data"
done
