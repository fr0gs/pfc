#!/bin/bash

# Dummy script to copy all data files corresponding to a single kernel version into
# the by_kernel/ directory
for i in `ls`
do
	cd $i
	for k in `ls`
	do
			if [[ $k == *$1*  ]];then
					cp $k ../by_kernel/$1/
			fi
	done
	cd - &>/dev/null
done
