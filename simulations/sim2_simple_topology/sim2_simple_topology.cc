
#include "ns3/network-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/dce-module.h"
#include "ns3/applications-module.h"
#include "ns3/drop-tail-queue.h"
#include "ns3/codel-queue.h"
#include "ns3/csma-module.h"
#include "../include/util.h" //This is shit, but I cannot touch the include path so..

#include <fstream>

#define DEBUG 1

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("sim2_simple_topology");

int main (int argc, char *argv[])
{
	string bytes = "5M";
	string qtype = "droptail";
	string algorithm = "reno";
	double error = 0;
	int run = 1;
	//int run = getRandInteger(1, 100, time(NULL));	

  CommandLine cmd;
	cmd.AddValue ("bytes", "Number of bytes", bytes);
	cmd.AddValue ("algorithm", "Congestion control algorithm", algorithm);
	cmd.AddValue ("error", "Error Rate to simulate", error);
	cmd.AddValue ("run", "Run Number", run);
	cmd.AddValue ("qtype", "Queue type: droptail | codel", qtype);

  cmd.Parse (argc, argv);


	//Change the run number for the RNG in the simulator. Here you are guaranteed that those are 
	//completely independent executions
	RngSeedManager::SetRun(run);

	int totalNodes = 4;


  //Creates all the nodes together.
  NodeContainer nodes;
  nodes.Create (totalNodes);
	
  NodeContainer leftNodes = NodeContainer (nodes.Get(0), nodes.Get(1));
	NodeContainer centralNodes = NodeContainer (nodes.Get(1), nodes.Get(2)); 
	NodeContainer rightNodes = NodeContainer (nodes.Get(2), nodes.Get(3));

  
#ifdef KERNEL_STACK
  //Installs the linux kernel in all nodes.
  DceManagerHelper dceManager;
  dceManager.SetNetworkStack ("ns3::LinuxSocketFdFactory", "Library", StringValue ("liblinux.so"));
  dceManager.Install (nodes);

  LinuxStackHelper stack;
  stack.Install (nodes);
#else
  NS_LOG_ERROR ("Linux kernel stack for DCE is not available. build with dce-linux module.");
  return 0;
#endif
	
	CsmaHelper csma;
	NetDeviceContainer leftDevices, centralDevices, rightDevices;

	//Set the left channel attributes
	csma.SetChannelAttribute ("DataRate", StringValue ("1Gbps"));
  csma.SetChannelAttribute ("Delay", StringValue ("1ms"));
	leftDevices = csma.Install (leftNodes);
	
	//Set the central channel attributes
	csma.SetChannelAttribute ("DataRate", StringValue("200Mbps"));
	csma.SetChannelAttribute ("Delay", StringValue("98ms"));	
	centralDevices = csma.Install(centralNodes);

	//Set the right channel attributes
	csma.SetChannelAttribute ("DataRate", StringValue ("1Gbps"));
	csma.SetChannelAttribute ("Delay", StringValue("1ms"));
	rightDevices = csma.Install(rightNodes);


	//Get the CsmaNetDevice associated to the first router.
	//ESTEBAN no te confundas, centralDevices.Get(0) es el PRIMER ROUTER!!!
	Ptr<NetDevice> dev = centralDevices.Get(0);
	Ptr<CsmaNetDevice> router = dev->GetObject<CsmaNetDevice> ();

	Ptr<NetDevice> dev1 = centralDevices.Get(1);
	Ptr<CsmaNetDevice> router1 = dev1->GetObject<CsmaNetDevice> ();

	//Create the queue.
	Ptr<Queue> qt;
	
	if ( qtype == "droptail" ) {
		//Create a DropTailQueue
		qt = CreateObject<DropTailQueue> (); 
		//DropTailQueue uses packets by default.
	}
	else if ( qtype == "codel" ){
		//Create a CoDelQueue
		qt = CreateObject<CoDelQueue> ();
		qt->SetAttribute("Mode", StringValue("QUEUE_MODE_PACKETS")); //bytes by default
	}
	else {
		NS_LOG_ERROR ("ERROR: queue type not supported");
    exit(-1);
	}
	qt->SetAttribute ("MaxPackets", UintegerValue(100));

	//Install the queue in the router.
	router->SetQueue(qt);
	router1->SetQueue(qt);

	//Create a RateErrorModel for the receive device in Client and vary its param
	Ptr<RateErrorModel> em = CreateObject<RateErrorModel> ();
	
	em->SetAttribute ("ErrorRate", DoubleValue (error));
	em->SetAttribute ("ErrorUnit", EnumValue(RateErrorModel::ERROR_UNIT_PACKET)); //byte by default

	//Install the RateErrorModel both central routers.
	centralDevices.Get (0)->SetAttribute ("ReceiveErrorModel", PointerValue (em));
	//centralDevices.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em));
		
  //Assigns IP addresses to the three different networks.
  Ipv4AddressHelper address;
  
  address.SetBase("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer leftInterfaces =  address.Assign (leftDevices);
  
  address.SetBase("192.168.2.0", "255.255.255.0");
  Ipv4InterfaceContainer centralInterfaces = address.Assign (centralDevices);

	address.SetBase("192.168.3.0", "255.255.255.0");
	Ipv4InterfaceContainer rightInterfaces = address.Assign (rightDevices);


	//Populate routing tables.
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  LinuxStackHelper::PopulateRoutingTables ();

	string str;
	str = GetNodeIpv4Address (nodes.Get (totalNodes-1));

	ostringstream args;

  //Establish iperf client parameters
	args.str("");
	args.clear();
  args << "-c " << str << " -n " << bytes << " -F " << "/dev/urandom";

	//Launch iperf client 
 	RunIperf(nodes.Get (0), Seconds (0.7), Seconds (-1), args.str());

	//Launch iperf server
	RunIperf(nodes.Get (totalNodes-1), Seconds (0.5), Seconds(-1), "-s");

	csma.EnablePcapAll ("csma");

	//Set sysctl parameters
	stack.SysctlSet (nodes, ".net.ipv4.tcp_congestion_control", algorithm);

  LinuxStackHelper::SysctlGet (nodes.Get (0), Seconds (1),
                               ".net.ipv4.tcp_available_congestion_control", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (nodes.Get (0), Seconds (1),
                               ".net.ipv4.tcp_allowed_congestion_control", &PrintTcpFlags);
  LinuxStackHelper::SysctlGet (nodes.Get (0), Seconds (1),
                               ".net.ipv4.tcp_congestion_control", &PrintTcpFlags);
	


  Simulator::Stop (Hours (30));
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}
