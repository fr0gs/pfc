#!/bin/bash
# This script executes the sim2 simulation 12 times, gets the avg 
# tput/inflight/delay data, and creates a data file for each data file.
# Output is then copied to the specified directory.

dest=$1
alg=$2
error=$3

declare yellow='\033[1;33m'
declare cyan='\033[1;36m'
declare green='\033[1;32m'
declare nc='\033[0m'


if [ "$#" -ne 3 ]; then
	echo "Usage: $ ./sim2_averages.sh <dest_dir> <tcp_congestion_control> <error(%)>"
	exit -1
fi

direc=$dest"/"
if [ ! -d "$direc" ]; then
  mkdir $direc
fi

mkdir $direc


direc=$direc"/"$alg
if [ ! -d "$direc" ]; then
  mkdir $direc
fi


kernels=( "2.6" "3.7" "3.10" "3.12" ) #"3.14" )


for kernelver in "${kernels[@]}"
do
	echo -e "[+] ${green} Algorithm: ${nc} $alg"
	echo -e "[+] ${green} Kernel version: ${nc} $kernelver"
	
	cdmysc $kernelver
	cd sim2_simple_topology/
	
	mkdir temp

	declare destfichdelay="delay_"$alg"_"$kernelver
	declare destfichtput="tput_"${error: -1}"_"$alg"_"$kernelver
	declare destfichowin="owin_"${error: -1}"_"$alg"_"$kernelver
	
	if [ $kernelver == "2.6" ];
	then
		destfichtput=$destfichtput".36.data"
		destfichowin=$destfichowin".36.data"
		destfichdelay=$destfichdelay".36.data"
	else
		destfichtput=$destfichtput".0.data"
		destfichowin=$destfichowin".0.data"
		destfichdelay=$destfichdelay".0.data"
	fi

	rm -f $destfichdelay &>/dev/null
	rm -f $destfichtput &>/dev/null
	rm -f $destfichowin &>/dev/null

	touch $destfichdelay
	touch $destfichtput
	touch $destfichowin

	for i in `seq 1 12`
	do
		#Execute the simulation
		echo -e "${yellow}[->] ${cyan} Execute simulation number: ${nc} $i"
		wafdce --run "sim2_simple_topology --error=$error --algorithm=$alg --run=$i" 
		
		echo -e "${yellow}[->] ${cyan} tput graph for iteration: ${nc} $i"
		captcp throughput -f 1.1 -u kilobyte -i -o temp/ csma-0-0.pcap &>/dev/null 
		tpinf-stat temp/throughput.data | awk -v var=$i 'END { print $3"\t"var }' >> $destfichtput

		rm -f temp/*
		
		echo -e "${yellow}[->] ${cyan} inflight data graph (owin) for iteration: ${nc} $i"
		captcp inflight -f 1.1 -i -o temp/ csma-0-0.pcap &>/dev/null
		tpinf-stat temp/inflight.data | awk -v var=$i 'END { print $3"\t"var }' >> $destfichowin

		echo -e "${yellow}[->] ${cyan} tsg graph for iteration: ${nc} $i"
		captcp timesequence -f 1.1 -o temp/ -i -e csma-0-0.pcap &>/dev/null
		tsg=$(cat temp/seq.data | awk 'END { print $1 }')
		echo $tsg >> $destfichdelay
	done
	
	echo -e "${green}************************************************${nc}"

	rm -rf temp/
	mv $destfichtput $direc
	mv $destfichowin $direc
	mv $destfichdelay $direc

done

unset direc
unset destfichtput
unset destfichowin
