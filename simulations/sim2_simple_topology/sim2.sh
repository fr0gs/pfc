#!/bin/bash

# reno bic cubic westwood highspeed hybla htcp vegas veno scalable lp yeah illinois


alg=$1
dest=$2
error=$3

if [ "$#" -ne 3 ]; then
	echo "Usage: $ ./sim2.sh <tcp_congestion_control> <dest_dir> <error(%)>"
	exit -1
fi
 
direc=$dest"/"
if [ ! -d "$direc" ]; then
  mkdir $direc
fi

direc=$direc"sim2_simple_topology/"
if [ ! -d "$direc" ]; then
  mkdir $direc
fi

direc=$direc$alg
if [ ! -d "$direc" ]; then
  mkdir $direc
fi


kernels=( "2.6" "3.7" "3.10" "3.12" ) #"3.14" )

for kernelver in "${kernels[@]}"
do
	echo "[+] Change to directory: "$kernelver
	fin=""
	cdmysc $kernelver
	cd sim2_simple_topology/
	echo "[+] Executes simulation"
	make clean
	wafdce --run "sim2_simple_topology --error=$error --algorithm=$alg"
	echo "[+] Creates graphs ( folder graphs/ )"
	graph_generator.sh csma-0-0.pcap
	if [[ $kernelver == "2.6" ]];
	then
		fin=".36"
	else
		fin=".0"
	fi

	dirfinal=$direc"/"$kernelver$fin

	if [ ! -d "$dirfinal" ]; then
  	mkdir $dirfinal
	fi

	echo "[+] Copy graphs/ to: "$direc
	cp -Rf graphs/ $dirfinal
	echo -e "\e[1;34m +++++++++++++++++++++++++ \e[0m"
done

unset fin
unset alg
unset direc
unset dest
unset error
