# README #
## Short Transfer Simulation ##

### Description ###
* Iperf to generate traffic.
* This is a simulation of 4 nodes trying to emulate a high speed congested link.
    Client <-> R1 ---- R2 <-> Server
* Packet loss will be of 0% - 5%. 
* For each execution the RNG seed will be initialized to a random value between 1-100.
    Seed is initialized in C++ using it's builtin random generation api, but is also
    accepted as a commandline parameter in case /dev/random, $RANDOM, shuf or any other
    utility is wanted to be used.
* Queue disciplines: Droptail.
* Queue size: 100, first router.
* Fixed file size: 5Mb
* MSS = 1460 bytes

Measure:
For each kernel version (graphs): 2.6 / 3.7 / 3.10 / 3.12
1. Delay
2. Throughput
3. Time sequence


### Install ###
* Copy the **include/** folder in the **myscripts/** directory in your ns-3-dce project.
* Copy the **sim2_simple_topology/** folder to the **myscripts/** folder in your ns-3-dce project.
* Copy **graph_generator.sh** into **/usr/bin/** or create a function that calls it wherever it is.
	This script uses **captcp** and **tcptrace** tools to generate the graphs.
* Copy **tpinf-stat** jar file into **/usr/bin** , or create a function that calls it wherever it is, mine is:

        function tpinf-stat {
		  local CWD="$PWD"
		  cd $PFCDIR"/scripts/"
		  java -jar tpinf-stat $CWD"/""$@"
		  cd - &>/dev/null
	    }
	    export -f tpinf-stat

* Have **wafdce** function defined in .bashrc, this function makes you able to execute a wafdce script from
  the same directory where you are.

        function wafdce {
    		local CWD="$PWD"
    		local DCENS=$(pwd | awk -F'/'  '{ for(i = 1; i <= NF-2; i++) printf("%s%s", $i, i == NF - 2 ? "\n" : OFS)}' OFS="/")"/"
    		cd $DCENS
    		./waf --cwd="$CWD" "$@" 
    		cd - &>/dev/null 
	    }
	    export -f wafdce

### Execute ###
* **Sim2.sh** executes sim2_simple_topology simulation in all four kernel versions with a given congestion 
  control algorithm and percentage of error. 
	Each kernel version is in a different directory with it's own **myscripts/** directory. It is structured
	in a way where there is four copies of the ns-3-dce project, each one using a different kernel version.
	This is done this way in order to speed up the execution of tests when changing from one version to
	the other, having to deal with the nuisance of four copies of the simulation distributed among projects is 
	always better than rebuilding project each time with a different kernel, which in turn I am not sure
	if that can occur without arising several errors when re-executing simulations. So, approach structure is this way:

			2.6/myscripts/simulation
			3.7/myscripts/simulation
			3.10/myscripts/simulation
			3.12/myscripts/simulation
	
	The **cdmysc** function called in the script just changes from one **myscripts/** directory to the other. Routes will change
  between computers, but this is mine as an orientation:

	    function cdmysc {
		    local cdir=$HOME"/gits/proyecto_"
		    if [[ $1 == "2.6" || $1 == "3.7" || $1 == "3.10" || $1 == "3.12" || $1 == "3.14" ]]; then
			    if [[ $1 == "2.6"  ]]; then
				    cdir=$cdir$1".36/source/ns-3-dce/myscripts/"
			    else
				    cdir=$cdir$1".0/source/ns-3-dce/myscripts/"
			    fi
		    cd $cdir
		    else
			    echo "Error cdmysc: unknown directory value"
		    fi
	    }
	    export -f cdmysc

	    $ ./sim2.sh <tcp_congestion_control> <dest_dir> <error(%)>

* **Sim2_averages.sh** executes the sim2_simple_topology simulation 12 times, gets the average
  throughput/inflight data segments/delay data, and creates a data file for each one.
  Output is then copied to the specified directory. 

* For a single execution of the simulation:

	    $ wafdce --run "sim2_simple_topology"

		sim2_simple_topology [Program Arguments] [General Arguments]

		Program Arguments:
			--bytes:      Number of bytes [5M]
			--algorithm:  Congestion control algorithm [reno]
			--error:      Error Rate to simulate [0]
			--run:        Run Number [1]
			--qtype:      Queue type: droptail | codel [droptail]
	```
