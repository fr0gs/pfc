# README #

This is a repository where all the tools, scripts and simulations written during the course of my thesis will be gathered.


### What can you find in this repository? ###
* **tcpbyter**: is a client/server program written in C created to provide a simple way for testing TCP connections in the NS3 simulator with the DCE native execution framework. It aims to be much simpler that iperf/netperf, and new features will be added as long as I'll need them.
* **tpinf-stat**: java program created to receive a two-column data file and shows some simple statistic information about it (I am using it to measure throughput data files). 
* Useful scripts in **scripts/** directory that helped to speed up simulations and analysis.
* Several simulations in **c++** for the ns3 simulator with the dce framework. 

### Simulations ###
* **sim2_simple_topology**: simple four node C - R1 - R2 - S topology to act as a starting point.
* **sim3_multi_flow**: variation of the sim2_simple_topology by adding multiple clients to the C side that act as competing flows to R1.