#!/bin/bash

# Find in directory "."
# Regular files "-type f"
# With the name "-name plot.plt"
# Print directory where they are placed "-printf '%h\n'"
# Execute gnuplot to it and copy

rm graphs/*

echo "[+] Finding all the plot.plt files and plotting them"
find . -type f -name '*.plt' -printf '%h\n' | xargs -i bash -c 'cd $0; gnuplot *.plt; cd -' {}
echo "[+] Copying all PNG graphics in graphs/ directory"
find . -wholename "./data_graph/measures_simulator_time" -prune -o -type f -name '*.png' -printf '%p\n' | xargs -i bash -c 'cp $0 graphs/' {}



